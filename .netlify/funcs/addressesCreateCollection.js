import faunadb from 'faunadb'
import jwt from "jsonwebtoken";

exports.handler = async (event, context, callback) => {

    const {REACT_APP_JWT_SECRET} = process.env;

    const token = event.headers.authorization.split(" ")[1];
    const checktoken = jwt.verify(token, REACT_APP_JWT_SECRET)

    const q = faunadb.query;
    const client = new faunadb.Client({
        secret: checktoken.fdb
    });

    const collectionName = `addresses`;
    const indexAllName = `all_addresses`;

    try {
        let res = await client.query(q.Exists(q.Collection(collectionName)))

        if (!res) {
            let cc = await client.query(q.CreateCollection({name: collectionName}));
            if (cc) {
                let ci = await client.query(q.CreateIndex(
                    {
                        name: indexAllName,
                        source: q.Collection(collectionName)
                    },
                ));
                if (ci) {
                    return {
                        statusCode: 200,
                        body: JSON.stringify("collection created with all_index")
                    }
                } else {
                    return {
                        statusCode: 200,
                        body: JSON.stringify("collection created without all_index")
                    }
                }
            }
        } else {
            return {
                statusCode: 200,
                body: JSON.stringify("collection already exists")
            }
        }
    } catch (e) {
        return {
            statusCode: 422,
            body: String(e)
        }
    }



};