import faunadb from 'faunadb'


exports.handler = async (event, context, callback) => {


    const q = faunadb.query;
    const client = new faunadb.Client({
        secret: process.env.REACT_APP_FAUNADB_SECRET
    });

    console.log(JSON.parse(event.body))


    //create database for this user
    const dbName = `stopLoss_${JSON.parse(event.body).gId}`;
    const userDb = await client.query(q.CreateDatabase({name: dbName}));

    //create key for this database
    const userDbKey = await client.query(q.CreateKey({database: q.Database(dbName), role: 'admin'}));
    const key = userDbKey.secret;

    //finally create user
    const dataUser = JSON.parse(event.body);
    const userItem = {
        data: {...dataUser, faunaSecret: key}
    };
    return client.query(q.Create(q.Ref('classes/stopLoss_users'), userItem))
        .then((response) => {
            return  {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        }).catch(error => ({ statusCode: 422, body: String(error) }));
};