import faunadb from 'faunadb'
import jwt from "jsonwebtoken";

exports.handler = async (event, context, callback) => {

    const {REACT_APP_JWT_SECRET} = process.env;

    const token = event.headers.authorization.split(" ")[1];
    const checktoken = jwt.verify(token, REACT_APP_JWT_SECRET)

    const q = faunadb.query;
    const client = new faunadb.Client({
        secret: checktoken.fdb
    });

    const pair = event.queryStringParameters.pair;
    const collectionName = `tradeData-${pair}`;
    const indexAllName = `all_tradeplans-${pair}`;

    let today = new Date().getTime();
    //let todayFormatted = today.getFullYear() + "-" + (today.getMonth()+1) + "-" + today.getDate();
    return client.query(q.Update(q.Collection(collectionName), {name: `${collectionName}_${today}`})).then((r) => {

        return client.query(q.Update(q.Index(indexAllName), {name: `${indexAllName}_${today}`})).then((i) => {
            return {
                statusCode: 200,
                body: JSON.stringify("collection renamed")
            }
        }).catch(err => ({statusCode:422, body: JSON.stringify(err) }));

    }).catch(err => ({statusCode:422, body: JSON.stringify(err) }));

};