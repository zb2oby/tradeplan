import faunadb from 'faunadb'
import jwt from "jsonwebtoken";

exports.handler = async (event, context, callback) => {

    const {REACT_APP_JWT_SECRET, REACT_APP_FAUNADB_SECRET} = process.env;

    const token = event.headers.authorization.split(" ")[1];
    const checktoken = jwt.verify(token, REACT_APP_JWT_SECRET)
    const userId = checktoken.gId;

    const q = faunadb.query;
    const client = new faunadb.Client({
        secret: REACT_APP_FAUNADB_SECRET
    });

    let data = JSON.parse(event.body);

    //prevent erase non updated data
    Object.keys(data).forEach((key,index)  => {
        if (data[key] === "") {
            data[key] = checktoken[key]
        }
    });

    const user = await client.query(q.Map(q.Paginate(q.Match(q.Index(`findByGId`),  userId)), q.Lambda("user", q.Get(q.Var("user")))))
    if (user.data.length > 0) {
        const userRef = user.data[0].ref;
        return client.query(q.Update(userRef, {data: data})).then((response) => {
            return {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        }).catch(error => ({ statusCode: 422, body: String(error) }));
    }

};