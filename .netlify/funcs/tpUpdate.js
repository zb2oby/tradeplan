import faunadb from 'faunadb'
import jwt from "jsonwebtoken";

exports.handler = async (event, context, callback) => {

    const {REACT_APP_JWT_SECRET} = process.env;

    const token = event.headers.authorization.split(" ")[1];
    const checktoken = jwt.verify(token, REACT_APP_JWT_SECRET)

    const q = faunadb.query;
    const client = new faunadb.Client({
        secret: checktoken.fdb
    });

    const data = JSON.parse(event.body)
    const id = event.queryStringParameters.id;
    const pair = event.queryStringParameters.pair;
    return client.query(q.Update(q.Ref(q.Collection(`tradeData-${pair}`),`${id}`), {data}))
        .then((response) => {
            return {
                statusCode: 200,
                body: JSON.stringify(response)
            }
        }).catch(error => ({ statusCode: 422, body: String(error) }));
};