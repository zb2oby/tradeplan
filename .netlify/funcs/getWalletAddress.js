import faunadb from 'faunadb'
import jwt from "jsonwebtoken";

exports.handler = async (event, context, callback) => {

    const {REACT_APP_JWT_SECRET} = process.env;

    const token = event.headers.authorization.split(" ")[1];
    const checktoken = jwt.verify(token, REACT_APP_JWT_SECRET)

    const q = faunadb.query;
    const client = new faunadb.Client({
        secret: checktoken.fdb
    });

    const pair = event.queryStringParameters.pair;
    const walletCurrency = pair.split('-')[0];


    try {
        const index = await client.query(q.Exists(q.Index(`all_addresses`)));

        if (index) {
            return client.query(q.Paginate(q.Match(q.Ref(`indexes/all_addresses`))))
                .then((response) => {
                    const todoRefs = response.data;
                    const getAllInitDataQuery = todoRefs.map((ref) => {
                        return q.Get(ref)
                    });
                    // then query the refs
                    return client.query(getAllInitDataQuery).then((ret) => {
                        let address = ret.find((r) => r.data.currency === walletCurrency);
                        if (address) {
                            return {
                                statusCode: 200,
                                body: JSON.stringify(address.data.address)
                            }
                        } else {
                            return {
                                statusCode: 200,
                                body: JSON.stringify('')
                            }
                        }

                    }).catch(error => ({ statusCode: 422, body: String(error) }));
                }).catch(error => ({ statusCode: 422, body: String(error) }));
        } else {
            return {
                statusCode: 200,
                body: JSON.stringify('')
            }
        }
    }
    catch (e) {
        return {
            statusCode: 422,
            body: String(e)
        }
    }

};