import Base64 from "crypto-js/enc-base64";
import Hmac256 from "crypto-js/hmac-sha256";
import fetch from "node-fetch";
import jwt from 'jsonwebtoken';

exports.handler = (event, context, callback) => {

    const {REACT_APP_API_BASE_URL, REACT_APP_JWT_SECRET} = process.env;

    const token = event.headers.authorization.split(" ")[1];
    const checktoken = jwt.verify(token, REACT_APP_JWT_SECRET);
    const checkCBSecret = jwt.verify(checktoken.cbSecret, REACT_APP_JWT_SECRET);

    let consolidedPath = event.queryStringParameters.path;
    for (let param in event.queryStringParameters) {
        if (param !== "path") {
            consolidedPath += `&${param}=${event.queryStringParameters[param]}`
        }
    }

    fetch('https://api.pro.coinbase.com/time').then((res) => {
        res.json().then((json) => {
            const timestamp = json.epoch;
            const data      = event.body;
            const method    = event.httpMethod;
            const path      = consolidedPath;
            const body      = (method === 'GET' || !data) ? '' : JSON.stringify(data);
            const message   = timestamp + method + path + body;
            const key       = Base64.parse(checkCBSecret.secret);
            const hash      = Hmac256(message, key).toString(Base64);



            fetch(REACT_APP_API_BASE_URL + path,
                {
                    method: method,
                    headers: {
                        'CB-ACCESS-KEY': checktoken.cbAccess,
                        'CB-ACCESS-PASSPHRASE': checktoken.cbPass,
                        'CB-ACCESS-SIGN': hash,
                        'CB-ACCESS-TIMESTAMP': timestamp,
                        "Content-Type": "application/json",
                        "Access-Control-Allow-Origin": "*"
                    }
                })
                .then(response => response.json()).then(dataRes => {
                callback(null, {
                    statusCode: 200,
                    body: JSON.stringify(dataRes)
                })
            }).catch(error => (callback(null,{ statusCode: 422, body: String(error) })));
        })

    }).catch(error => (callback(null,{ statusCode: 422, body: String(error) })));

};