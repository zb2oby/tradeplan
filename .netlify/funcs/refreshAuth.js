import jwt from "jsonwebtoken";
import faunadb from 'faunadb';


exports.handler = async (event, context, callback) => {

    const {REACT_APP_JWT_SECRET, REACT_APP_FAUNADB_SECRET} = process.env;

    const q = faunadb.query;
    const clientfdb = new faunadb.Client({
        secret: REACT_APP_FAUNADB_SECRET
    });

    try {
        const userId = event.body;

        const user = await clientfdb.query(q.Map(q.Paginate(q.Match(q.Index(`findByGId`), userId)), q.Lambda("user", q.Get(q.Var("user")))))
        if (user.data.length > 0) {
            const userData = user.data[0].data;
            const userRef = user.data[0].ref;

            const refresh = jwt.verify(userData.refreshToken, REACT_APP_JWT_SECRET);
            const isChecked = refresh.gId === userData.gId;
            if (isChecked) {
                //creation du token d'accès
                let jwtToken = await jwt.sign({
                    fdb: userData.faunaSecret,
                    cbAccess: userData.cbAccess,
                    cbPass: userData.cbPass,
                    cbSecret: userData.cbSecret,
                    gId: userData.gId
                }, REACT_APP_JWT_SECRET, {
                    expiresIn: "1h"
                });

                //création du token de raffraichissement et update du user
                let refreshToken = await jwt.sign({
                    gId: userData.gId
                }, REACT_APP_JWT_SECRET, {
                    expiresIn: "24h"
                });
                const updatedUserData = {...userData, refreshToken: refreshToken}
                await clientfdb.query(q.Update(userRef, {data: updatedUserData}));

                return {
                    statusCode: 200,
                    body: JSON.stringify({token: jwtToken, check: isChecked, favorite: userData.favorite, defaultCurr: userData.defaultCurr})
                }
            }

            return {
                statusCode: 200,
                body: JSON.stringify({gId: payload['sub'], check: false})
            }


        }
    } catch (e) {
        return {
            statusCode: 422,
            body: String(e)
        }
    }
};