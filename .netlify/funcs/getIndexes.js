import faunadb from 'faunadb'
import jwt from "jsonwebtoken";

exports.handler = async (event, context, callback) => {

    const {REACT_APP_JWT_SECRET} = process.env;

    const token = event.headers.authorization.split(" ")[1];
    const checktoken = jwt.verify(token, REACT_APP_JWT_SECRET)

    const q = faunadb.query;
    const client = new faunadb.Client({
        secret: checktoken.fdb
    });

    const pair = event.queryStringParameters.pair;
    const indexPrefix = `all_tradeplans-${pair}`;;


       return client.query(q.Map(q.Paginate(q.Indexes()), q.Lambda("Col", q.Get(q.Var("Col"))))).then((res) => {
            if (res.data.length > 0) {
                let indexRefs = res.data.filter((c) => c.name.includes(indexPrefix) && c.name !== indexPrefix);

                return {
                    statusCode: 200,
                    body: JSON.stringify(indexRefs)
                }
            } else {
                return {
                    statusCode: 200,
                    body: JSON.stringify("Aucune collection trouvée")
                }
            }

        }).catch(err => ({statusCode:422, body: JSON.stringify(err) }));

};