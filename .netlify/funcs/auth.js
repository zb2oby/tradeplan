import {OAuth2Client} from 'google-auth-library';
import faunadb from 'faunadb';
import jwt from 'jsonwebtoken';

exports.handler = (event, context, callback) => {

    const {REACT_APP_GOOGLE_SIGNIN_ID, REACT_APP_JWT_SECRET, REACT_APP_FAUNADB_SECRET} = process.env;

    async function verify() {

        const client = new OAuth2Client(REACT_APP_GOOGLE_SIGNIN_ID);

        const q = faunadb.query;
        const clientfdb = new faunadb.Client({
            secret: REACT_APP_FAUNADB_SECRET
        })

        const token = event.body;

        const ticket = await client.verifyIdToken({
            idToken: token,
            audience: REACT_APP_GOOGLE_SIGNIN_ID
        });

        const payload = ticket.getPayload();
        //console.log(payload)

        const user = await clientfdb.query(q.Map(q.Paginate(q.Match(q.Index(`findByGId`),  payload['sub'])), q.Lambda("user", q.Get(q.Var("user")))))
        if (user.data.length > 0) {
            const userData = user.data[0].data;
            const userRef = user.data[0].ref;
            //console.log(userData)
            const isChecked = payload['sub'] === userData.gId;


            //creation du token d'accès
            let jwtToken = await jwt.sign({
                fdb: userData.faunaSecret,
                cbAccess: userData.cbAccess,
                cbPass:userData.cbPass,
                cbSecret:userData.cbSecret,
                gId: userData.gId
            }, REACT_APP_JWT_SECRET, {
                expiresIn: "1h"
            });

            //création du token de raffraichissement
            let refreshToken = await jwt.sign({
                gId: userData.gId
            }, REACT_APP_JWT_SECRET, {
                expiresIn: "24h"
            });
            const updatedUserData = {...userData, refreshToken: refreshToken}
            await clientfdb.query(q.Update(userRef, {data: updatedUserData}));

            callback(null, {
                statusCode: 200,
                body: JSON.stringify({token: jwtToken, check: isChecked, favorite: userData.favorite, defaultCurr: userData.defaultCurr})
            })
        } else {
            callback(null, {
                statusCode: 200,
                body: JSON.stringify({gId: payload['sub'], check: false})
            })
        }
    }

    verify().catch((e) => {
        console.log(e)
        callback(null, {
            statusCode: 422,
            body: String(e)
        })
    });


}