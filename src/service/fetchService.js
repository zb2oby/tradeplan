import {WEB_SOCKET_FEED_URI} from "./constants";
import computeSignature from "./CBSign";
import { w3cwebsocket as W3CWebSocket } from "websocket";
import {checkAndrefreshExpireToken} from "../utils";


export const httpRequest = (options, trace = true) => {



    let url = process.env.REACT_APP_NETLIFY_BACK_URL + options.path;

    let infoPath = options.path;
    if (options.from && options.from !== "") {
        infoPath += " : FROM " + options.from;
    }
    delete options.path;

    if (navigator.onLine) {
        return checkAndrefreshExpireToken().then((token) => {

            let forceCall = options.forceCall;
            delete options.forceCall;
            //si la session existe deja on est dans le cas d'un refresh, on envio donc la nouvelle session
            if (infoPath === 'auth' && sessionStorage.getItem('stoploss') !== null && !forceCall) {
                return {refresh: true}
            }

            //sinon on effectue l'appel
            const headers = new Headers({});

            if (token !== null && token !== undefined) {
                headers.append('Authorization', 'Bearer ' + token)
            }
            options = {...options, headers: headers};

            return fetch(url, options).then(response =>
                response.json().then(json => {
                    if (trace) {
                        console.log("%c" + infoPath, "background-color: #ccc; padding:2px 7px; margin-bottom: 7px; border-radius: 15px; color:#fff;", json);
                    }
                    if (json.message) {
                        return Promise.reject(json);
                    } else {
                        return json;
                    }
                })
            );

        });

    }

};

export const webSocketRequest = (options) => {

    if (navigator.onLine &&     options.products.length !== 0 && sessionStorage.getItem('stoploss') !== null) {


        return  computeSignature({
            path: `/users/self/verify`,
            method: 'GET'
        }).then((userVerify)=> {
            const client = new W3CWebSocket(WEB_SOCKET_FEED_URI);
            const data =   {
                type: options.type,
                product_ids: options.products,
                channels: options.channels,
                signature: userVerify.sign,
                key: userVerify.cbAccess,
                passphrase: userVerify.cbPass,
                timestamp: userVerify.timestamp.toString()
            };



            client.onopen = () => {
                console.log('Socket is open, sending data...')
                if (client.readyState === 1) {
                    client.send(JSON.stringify(data))
                }

            }
            return client;

        });



    }


};