import Hmac256 from 'crypto-js/hmac-sha256';
import Base64 from 'crypto-js/enc-base64';
import {getCBTimestamp} from "./apiCalls";
import jwt from "jsonwebtoken";
import {checkAndrefreshExpireToken} from "../utils";

export default async function computeSignature(request) {

    return await checkAndrefreshExpireToken().then((token) => {
        const checktoken = jwt.verify(token, process.env.REACT_APP_JWT_SECRET);
        const checkCBSecret = jwt.verify(checktoken.cbSecret, process.env.REACT_APP_JWT_SECRET);
        /*= ( Date.now() / 1000)*/
        return getCBTimestamp().then((res)=> {
            const timestamp = res.epoch;
            const data      = request.data;
            const method    = request.method;
            const path      = request.path;
            const body      = (method === 'GET' || !data) ? '' : JSON.stringify(data);
            const message   = timestamp + method + path + body;
            const key       = Base64.parse(checkCBSecret.secret);
            const hash      = Hmac256(message, key).toString(Base64);
            return {timestamp: timestamp, sign: hash, cbAccess: checktoken.cbAccess, cbPass: checktoken.cbPass};
        });

    });



}