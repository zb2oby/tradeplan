import {httpRequest, webSocketRequest} from "./fetchService";


export function checkAuth(token, forceCall) {
    return httpRequest({
        path: `auth`,
        method: 'POST',
        body: token,
        forceCall: forceCall
    })
}

export function signup(data) {
    return httpRequest({
        path: `signup`,
        method: 'POST',
        body: JSON.stringify(data)
    })
}

//*****COINBASE*****//

export function getCBTimestamp() {
    return fetch('https://api.pro.coinbase.com/time').then((res) => res.json()).catch((err) => console.log(err))
}

export function getCurrentPrice(productIds) {
    return webSocketRequest({
        type: "subscribe",
        products: productIds,
        channels: ["ticker"]
    })
}

export function getAccount(from) {
    return httpRequest({
        path: `hideMe?path=/accounts`,
        method: 'GET',
        from: from
    })
}


export function getCoinbaseAccounts() {
    return httpRequest({
        path: `hideMe?path=/coinbase-accounts`,
        method: 'GET',
    })
}

export function createAddressesCollectionIfNotExists() {
    return httpRequest({
        path: `addressesCreateCollection`,
        method: 'GET'
    })
}

export function createAddress(data) {
    return httpRequest({
        path: `createWalletAddress`,
        method: 'POST',
        body: JSON.stringify(data)
    })
}

export function generateAddress(accountId) {
    return httpRequest({
        path: `hideMe?path=/coinbase-accounts/${accountId}/addresses`,
        method: 'POST',
        body: ''
    })
}

export function getCoinbaseLedger(accountId) {
    return httpRequest({
        path: `hideMe?path=/accounts/${accountId}/ledger`,
        method: 'GET',
    })
}

export function getExistingAddress(pair) {
    return httpRequest({
        path: `getWalletAddress?pair=${pair}`,
        method: 'GET'
    })
}

export function getFillHistory2(productId) {
    return httpRequest({
        path: `hideMe?path=/fills?product_id=${productId}`,
        method: 'GET'
    })
}

export function getOrder(orderId) {
    return httpRequest({
        path: `hideMe?path=/fills?order_id=${orderId}`,
        method: 'GET'
    })
}


export function getTransfersList() {
    return httpRequest({
        path: `hideMe?path=/transfers`,
        method: 'GET'
    })
}


export function getCandles(start, end, granularity, pair, from) {
    return httpRequest({
        path: `hideMe?path=/products/${pair}/candles?start=${start}&end=${end}&granularity=${granularity}`,
        method: 'GET',
        from: from
    }, true)
}


//****FAUNADB****//

export function createTPCollectionIfNotExists(pair) {
    return httpRequest({
        path: `tpCreateCollection?pair=${pair}`,
        method: 'GET'
    })
}


export function archiveTPCollection(pair) {
    return httpRequest({
        path: `tpRenameCollection?pair=${pair}`,
        method: 'GET'
    })
}

export function getArchives(pair) {
    return httpRequest({
        path: `getIndexes?pair=${pair}`,
        method: 'GET'
    })
}

export function createTradePlan(data, pair) {
    return httpRequest({
        path: `tpCreate?pair=${pair}`,
        method: 'POST',
        body: JSON.stringify(data)
    })
}



export function readAllTradePlan(index, from) {
    return httpRequest({
        path: `tpReadall?index=${index}`,
        method: 'GET',
        from: from
    })
}

export function updateTradePlan(id, data, pair) {
    return httpRequest({
        path: `tpUpdate?id=${id}&pair=${pair}`,
        method: 'POST',
        body: JSON.stringify(data)
    })
}




export function deleteTradePlan(id, pair) {
    return httpRequest({
        path: `tpDelete?id=${id}&pair=${pair}`,
        method: 'POST'
    })
}

export function updateUser(data) {
    return httpRequest({
        path: `userUpdate`,
        method: 'POST',
        body: JSON.stringify(data)
    })
}

/*HISTORY*/

export function createHistoryCollectionIfNotExists() {
    return httpRequest({
        path: `historyCreateCollection`,
        method: 'GET'
    })
}

export function updateHistory(id, data, pair) {
    return httpRequest({
        path: `historyUpdate?id=${id}&pair=${pair}`,
        method: 'POST',
        body: JSON.stringify(data)
    })
}

export function createHistory(data, pair) {
    return httpRequest({
        path: `historyCreate?pair=${pair}`,
        method: 'POST',
        body: JSON.stringify(data)
    })
}

export function readAllInit() {
    return httpRequest({
        path: `initReadAll`,
        method: 'GET'
    })
}

