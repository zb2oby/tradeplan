import jwt from "jsonwebtoken";
import {getCandles} from "./service/apiCalls";
import {currencies, supportedPairs} from "./components/Constants";
import PromiseThrottle from "promise-throttle";
import {LOG_DEBUG} from "./service/constants";


/**
 * Récupère les ordres courant c'est à dire les positions ouvertes
 * @param orders, l'historique complet des ordres de vente et d'achat
 * @returns {*|WordArray|Buffer|T[]|string}
 */
export const initPositionsFromOrders = (orders) => {
    //ranger les ordres d'achat et de vente dans deux tableaux différent trié par date
    let groupedOrders = groupBy(orders, "side");
    //pour chaque ordre de vente vérifier quel ordre d'achat anterieur il a clos et à quelle hauteur
    if (groupedOrders.sell) {
        console.log("SELLS", groupedOrders.sell)
        groupedOrders.sell.sort((a, b) => {
            let dateA = new Date(a.created_at);
            let dateB = new Date(b.created_at);
            return dateA - dateB
        }).forEach((sell) => {
            //découper les ordres d'achat en deux partie : les précédent à l'ordre de vente traité et les suivants
            let nextBuys = groupedOrders.buy.filter((buy) => {
                let dateBuy = new Date(buy.created_at);
                let dateSell = new Date(sell.created_at);
                return dateBuy > dateSell
            });
            let previousBuys = groupedOrders.buy.filter((buy) => {
                let dateBuy = new Date(buy.created_at);
                let dateSell = new Date(sell.created_at);
                return dateBuy < dateSell
            });
            //traitement des ordres d'achat précédent l'ordre de vente traité ==> si les ordres d'achat sont totalement clos alors les supprimer sinon les reduire

            let sellType = "internal";
            if (sell.sellType && sell.sellType === "external") {
                sellType = sell.sellType
            }
            let trigger = parsePositionsAndTriggerSell(sell.size, previousBuys, sellType);
            //à la fin les ordres d'achat restants sont ajouté au ordres d'acaht suivant l'ordre de vente qui a été traité
            groupedOrders = {...groupedOrders, buy: nextBuys.concat(trigger.newHistory)};
        });
    }

    //cette liste mise à jour constitue l'historique des positions en cours
    return groupedOrders.buy;
};


/**
 *
 * @param sizeToSell la taille à vendre
 * @param history la liste des investissement précédent l'ordre de vente (ce quon a a vendre en somme)
 * @param sellType le type de vente. vente type trade (interne à la plateforme). ou Achat externe avec ldes jetons (externe à la plateforme)
 * @param computedBuyPrice le prix auquel on a acheté ce qu'on vend (la somme des prix d'achat des ordres vendus)
 * @returns {{computedBuyPrice: *, newHistory: *}} objet contenant la somme des prix d'acchat des ordres qui on du être vendu pour honorer cet ordre de vente et l'historique mis à jour
 */
export function parsePositionsAndTriggerSell(sizeToSell, history, sellType = 'internal', computedBuyPrice = 0.00) {

    //on recherche le plus petit prix existant danss la liste
    if (history.length > 0) {

        let basePriceOrder;

        //si externe on va recherche le prix d'achat le plus elevé dans l'historique
        if (sellType === 'external') {
            let maxPriceOrder = history.reduce(
                (a, b) =>
                    parseFloat(a.price) > parseFloat(b.price)
                        ? a
                        : b
            );
            basePriceOrder = maxPriceOrder;
        }
        //si interne on va recherche le prix d'achat le plus bas dans l'historique
        if (sellType === 'internal') {
            let minPriceOrder = history.reduce(
                (a, b) =>
                    parseFloat(a.price) < parseFloat(b.price)
                        ? a
                        : b
            );
            basePriceOrder = minPriceOrder;
        }



        sizeToSell = parseFloat(sizeToSell);
        let priceOrderSize = parseFloat(basePriceOrder.size);
        let priceOrderPrice = parseFloat(basePriceOrder.price);
        let priceOrderFee = parseFloat(basePriceOrder.fee);

        //si la taille de ce prix est superieur à ce qu'il reste a vendre (toute la position n'est as vendue)
        if (priceOrderSize > sizeToSell) {
            computedBuyPrice += (sizeToSell * priceOrderPrice) + (sizeToSell / priceOrderSize * priceOrderFee); //
            //on met à jour la position de l'historique en lui enlevant ce qu'on vient d evendre
            history = history.map(invest => {
                if (invest.trade_id === basePriceOrder.trade_id) {
                    return {...invest, size: priceOrderSize - sizeToSell}
                } else {
                    return invest;
                }
            });

        }
        //sinon (si la taille de ce prix est inferieur à ce qu'il reste à vendre) ==> le pris d'achat correspond donc à la totalité de la position
        else {
            computedBuyPrice += (priceOrderSize * priceOrderPrice) + priceOrderFee;
            //on enleve la position de l'historque
            history = history.filter(invest => invest.trade_id !== basePriceOrder.trade_id);
        }


        //on enleve la taille vendue de la taille à vendre
        sizeToSell -= priceOrderSize;
        //tant que la taille a vendre est positive on continue
        if (sizeToSell > 0) {
            return parsePositionsAndTriggerSell(sizeToSell, history, sellType, computedBuyPrice);
        }
    }
    return {computedBuyPrice: computedBuyPrice, newHistory: history}

}


/**
 * Initialise et calcul les positions par rapport aux match negatif correspondant à des achat (donc qui ne sont pas des trade de vente)
 * @param product le produit des match
 * @param orders la liste entiere des ordres
 * @param matches les match negatifs (censés représenter des ventes "sell")
 * @param fees les fee
 * @returns {*}
 */
export function initOrdersFromMatches(product, orders, matches, fees = []) {

    //let initOrders = orders.filter((i) => i.product_id === pair);
    //on recupere la liste des ordre ayant permis d'obtenir le produit du match (donc ex si match = BTC on recupere tout les ordre type BTC-SMTHG)
    let ordersToConvert = orders.filter((o) => o.product_id.split('-')[0] === product && o.side === "buy");
    //recherche des match negatif qui ne sont pas des ordre de vente mais qui correspondent à des ordres d'achat donc à des conversions.
    let convertOrders = [];
    matches.forEach((negativeMatch) => {
        let findConversion = orders.find((o) => o.order_id === negativeMatch.details.order_id && o.side === "buy");
        if (findConversion) {
            convertOrders.push(negativeMatch);
        }
    });
    if (fees.length > 0) {
        fees.forEach((negativeMatch) => {
            let findConversion = orders.find((o) => o.order_id === negativeMatch.details.order_id && o.side === "buy");
            if (findConversion) {
                convertOrders.push(negativeMatch);
            }
        });
    }
    // console.log("LEDG ORD", convertOrders)

    if (convertOrders.length > 0) {
        convertOrders.forEach((toConvert) => {
            let sizeToParse = -parseFloat(toConvert.amount);
            let previousToConvert = ordersToConvert.filter((t) => new Date(t.created_at) < new Date(toConvert.created_at));
            ordersToConvert = parsePositionsAndTriggerConversion(sizeToParse, previousToConvert);
            //console.log("LEDG RES", ordersToConvert)
        });

        orders = orders.map((o) => {
            let toReturn  = o;
            if (ordersToConvert.length > 0) {
                ordersToConvert.forEach((c) => {
                    if (c.order_id === o.order_id) {
                        //console.log("LEDG CCC", c)
                        toReturn = c;
                    }
                });
            }
            return toReturn;
        })
    }
    return orders;
}

/**
 * Permet de traiter les ordres qui ne sont pas des trade ni des transfert mais des ordre de conversion d'une paire à une autre
 * @param sizeToSell la taille de l'ordre (match) traité
 * @param history la liste des ordres d'achat existants
 * @returns {*}
 */
export function parsePositionsAndTriggerConversion(sizeToSell, history) {

    //on prend le premier existante de la liste
    if (history.length > 0) {
        let lastOrder = history[0];


        sizeToSell = parseFloat(sizeToSell);
        let lastOrderSize = parseFloat(lastOrder.size);

        //si la taille de ce prix est superieur à ce qu'il reste a vendre (toute la position n'est as vendue)
        if (lastOrderSize > sizeToSell) {
            //on met à jour la position de l'historique en lui enlevant ce qu'on vient d evendre
            history = history.map(invest => {
                if (invest.trade_id === lastOrder.trade_id) {
                    return {...invest, size: lastOrderSize - sizeToSell, converted: true}
                } else {
                    return invest;
                }
            });

        }
        //sinon (si la taille de ce prix est inferieur à ce qu'il reste à vendre) ==> le pris d'achat correspond donc à la totalité de la position
        else {
            //on enleve la position de l'historque
            history = history.filter(invest => invest.trade_id !== lastOrder.trade_id);
        }

        //on enleve la taille vendue de la taille à vendre
        sizeToSell -= lastOrderSize;
        //tant que la taille a vendre est positive on continue
        if (sizeToSell > 0) {
            return parsePositionsAndTriggerConversion(sizeToSell, history);
        }
    }
    return history;

}

/**
 * Sépare un tableau en deux tableaux de valeurs groupée par rapport à une propriété du tableau de départ
 * @param arr
 * @param property
 * @returns {*}
 */
export function groupBy(arr, property) {
    return arr.reduce(function(memo, x) {
        if (!memo[x[property]]) { memo[x[property]] = []; }
        memo[x[property]].push(x);
        return memo;
    }, {});
}

/**
 * Affiche un resultat très petit avec une puissance
 * @param number
 * @param precision
 * @returns {*}
 */
export const displayRoundWith10ToMinus = (number, precision) => {
    //si le nombre arrondis est egal a zero
    let precisionRounded = "1"
    for (let i = 0; i < precision; i++) {
        precisionRounded += "0";
    }
    precisionRounded = parseFloat(precisionRounded);

    //console.log(precisionRounded)


    let rounded = Math.round(number * precisionRounded) / precisionRounded;
    //console.log(rounded)
    if (rounded === 0) {
        return number.toExponential(number, precision)
    }
    return rounded;

};

/**
 * Vérifie l'expiration du token et appel le back pour raffraichissement si necessaire
 * @returns {Promise<*>}
 */
export const checkAndrefreshExpireToken = async () => {
//check token validity
    if (sessionStorage.getItem('stoploss') !== null) {
        let token = JSON.parse(sessionStorage.getItem('stoploss')).token;
        return jwt.verify(token, process.env.REACT_APP_JWT_SECRET, async (err, decoded) =>{
            if (err) {
                if (err.name === "TokenExpiredError") {
                    const payload = jwt.verify(token, process.env.REACT_APP_JWT_SECRET, {ignoreExpiration: true} );
                    console.log("TokenExpiredError", "Raffraichissement du token")
                    let refreshToken = await fetch(`${process.env.REACT_APP_NETLIFY_BACK_URL}refreshAuth`, {method: 'POST', body: payload.gId})
                    try {
                        let res = await refreshToken.json();
                        console.log("%crefreshAuth", "background-color: #ccc; padding:2px 7px; margin-bottom: 7px; border-radius: 15px; color:#fff;", res);
                        sessionStorage.setItem('stoploss', JSON.stringify(res));
                        return res.token;
                    }
                    catch (err) {
                        console.log(err);
                    }
                }
            } else {
                return token
            }
        });
    }

};


export const convertUnknownDateFormatToISO = (date) => {
    //2019-08-19 19:34:49.786733+00
    let splitInit = date.split(" ");
    let dateString = splitInit[0];
    let splitHour = splitInit[1].split("+");
    let hourString = splitHour[0];

    return dateString + "T" + hourString + "Z";

};


export const detectMobileBrowser = () => {
    const toMatch = [
        /Android/i,
        /webOS/i,
        /iPhone/i,
        /iPad/i,
        /iPod/i,
        /BlackBerry/i,
        /Windows Phone/i
    ];

    return toMatch.some((toMatchItem) => {
        return navigator.userAgent.match(toMatchItem);
    });
};

export const checkUserAgentVisibility = () => {
    let hidden, visibilityChange;
    if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
        hidden = "hidden";
        visibilityChange = "visibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
        hidden = "msHidden";
        visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
        hidden = "webkitHidden";
        visibilityChange = "webkitvisibilitychange";
    }

    return {hidden: hidden, visibilityChange: visibilityChange}
};

export const promiseThrottle = new PromiseThrottle({
    requestsPerSecond: process.env.REACT_APP_TUNNING_THROTTLE,           // up to 3 request per second
    promiseImplementation: Promise  // the Promise library you are using
});


export const candlePromise = (searchDate, pairToSearch, outputObj, from) => {
    return new Promise(async resolve => {

        let output = outputObj.object;
        let errors = false;
        let granularity = 60;

        let timestamp = new Date(searchDate).getTime()/1000|0;

        try {
            //recherche ddu prix au moment de l'achat
            //on prend les bougies 30 minutes autour de la date souhaitée : 15 min avant - 15 min après
            let dateToStart = new Date(searchDate);
            dateToStart.setMinutes(dateToStart.getMinutes() - 15);
            let buyDateStart = dateToStart.toISOString();

            let dateToEnd = new Date(searchDate);
            dateToEnd.setMinutes(dateToEnd.getMinutes() + 15);
            let buyDateEnd = dateToEnd.toISOString();

            let price = 0.00;

            //TODO A surveiller avec dépot de jeton dont la paire avec defaultCurr (XTZ-EUR par ex) n'existe pas sur coinbase
            //récuperation du prix pour la paire transfromée si celle ci n'est pas supportée ==> sert pour la reup des prix defaultCurr (EUR) des différents dépôt
            //acount = XTZ
            //pairToSearch = XTZ-EUR ==> getCandle impossible cat paire inexistante sur coinbase
            //transformation XTZ-BTC ==> getCandle ok mais prix final mauvais il faudrais le prix du BTC-EUR à date si paire n'existe pas (non supportées)

            if (!isSupportedPair(pairToSearch)) {
                let supportedPairToSearch = transformToSupported(pairToSearch);

                //recherche du prix EUR pour BTC-EUR
                let buyPriceDef = await getCandles(buyDateStart, buyDateEnd, granularity, supportedPairToSearch.supportedDef, from);
                //on recupere dans le tableau l'entrée dont le timestemp est le plus proche de la date recherchée
                let closestSupportedPriceDef = buyPriceDef.reduce((a, b) => {
                    return Math.abs(b - timestamp) < Math.abs(a - timestamp) ? b : a;
                });

                //recherche du prix BTC pour XTZ-BTC
                let buyPriceCurr1 = await getCandles(buyDateStart, buyDateEnd, granularity, supportedPairToSearch.supportedCurr1, from);
                //on recupere dans le tableau l'entrée dont le timestemp est le plus proche de la date recherchée
                let closestSupportedPriceCurr1 = buyPriceCurr1.reduce((a, b) => {
                    return Math.abs(b - timestamp) < Math.abs(a - timestamp) ? b : a;
                });

                let priceDef = closestSupportedPriceDef[4];
                let priceCurr1 = closestSupportedPriceCurr1[4];

                output[outputObj.field]  = output[outputObj.field] * priceDef * priceCurr1
                //console.log("SEARCH", outputObj)

            } else {
                //recuperation de la paire recherchée
                let buyPrice = await getCandles(buyDateStart, buyDateEnd, granularity, pairToSearch, from);
                //on recupere dans le tableau l'entrée dont le timestemp est le plus proche de la date recherchée
                const closestPrice = buyPrice.reduce((a, b) => {
                    return Math.abs(b - timestamp) < Math.abs(a - timestamp) ? b : a;
                });

                price = closestPrice[4];
                output[outputObj.field] = output[outputObj.field] * price;
            }



           //console.log(timestamp, closestPrice)
            resolve(output)
        }
        catch(err) {
            console.log(err);
            errors = true;
        }
        finally {
            if (errors) {
                resolve(output)
            }
        }

    })

};

export const isFiat = (curr) => {
    let currency =  currencies.find((s) => s.curr === curr);
    if (currency) {
        return currency.type === "FIAT";
    }
    return false;
};

export const containFiat = (pair) => {
    let splittedPair = pair.split("-");

    let contains = false;
    splittedPair.forEach((p) => {
        let currency =  currencies.find((s) => s.curr === p);
        if (currency) {
            if (currency.type === "FIAT") {
                contains = true;
            }
        }
    });
    return contains;

}

export const isDefaultCurr = (curr) => {
    return JSON.parse(sessionStorage.getItem('stoploss')).defaultCurr === curr;
};

export const getSymbol = (curr) => {
    let currency =  currencies.find((s) => s.curr === curr);
    return currency.sym;
};

export const isSupportedPair = (pair) => {
    return supportedPairs.includes(pair);
};

export const isSupportedCurr = (curr) => {
    let curr0 = supportedPairs.map((pair) => pair.split('-')[0]);
    return curr0.includes(curr);
}

export const transformToSupported = (pair) => {
    let defaultCurr = JSON.parse(sessionStorage.getItem('stoploss')).defaultCurr;
    let getCurr1 = pair.split('-')[0];
    let findSupportedWithCurr1 = supportedPairs.filter((p) => p.split('-')[0] === getCurr1 && supportedPairs.includes(p.split('-')[1] + '-' + defaultCurr));
    return {supportedCurr1: findSupportedWithCurr1[0], supportedDef: findSupportedWithCurr1[0].split('-')[1] + '-' + defaultCurr}
};

export const checkLogLevel = () => {

    if(!LOG_DEBUG){
        if(!window.console) window.console = {};
        let methods = ["log", "debug", "warn", "info"];
        for(let i=0;i<methods.length;i++){
            console[methods[i]] = function(){};
        }
    }
};

export const hideModal = (modalId) => {

    /*const modal = document.getElementById(modalId);
    modal.removeAttribute("tabindex")
    modal.classList.remove('show');
    modal.setAttribute('aria-hidden', 'true');
    modal.removeAttribute("aria-modal");
    modal.removeAttribute("style");
    modal.style.display = "none";*/
    let buttonId = "dismiss-"+modalId;
    console.log("dismissId", buttonId)
    const buttonDismiss = document.getElementById(buttonId);
    console.log("dismiss", buttonDismiss)
    if (buttonDismiss !== null) {
        buttonDismiss.click();
    }
    /*var new_element = modal.cloneNode(true);
    modal.parentNode.replaceChild(new_element, modal);*/

    /*const modalBackdrops = document.getElementsByClassName('modal-backdrop');
    document.body.removeChild(modalBackdrops[0]);
    document.body.className = document.body.className.replace("modal-open","");
    document.body.removeAttribute("style")*/
}