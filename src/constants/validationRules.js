export const signupFormRules = {
    favorite: [
        {
            key: "favorite_empty",
            message: "Paire favorite obligatoire",
            isValid: (v) => v !== ''
        }
    ],
    defaultCurr: [
        {
            key: "default_empty",
            message: "Monnaie d'échange principale obligatoire",
            isValid: (v) => v !== ''
        }
    ]
};

export const tradePlanFormRules = {
    type: [
        {
            key: "type_empty",
            message: "Veuillez renseigner un type",
            isValid: (v) => v !== ''
        }
    ],
    quantite: [
        {
            key: "quantite_empty",
            message: "Veuillez indiquer une quantité en pourcentage",
            isValid: (v) => v !== ''
        }
    ],
    prix: [
        {
            key: "prix_empty",
            message: "Veuillez indiquer le prix d'achat ou de vente",
            isValid: (v) => v !== ''
        }
    ]
};


export const historyFormRules = {
    side: [
        {
            key: "side_empty",
            message: "Veuillez renseigner le type d'ordre",
            isValid: (v) => v !== ''
        }
    ],
    size: [
        {
            key: "size_empty",
            message: "Veuillez renseigner la taille",
            isValid: (v) => v !== ''
        }
    ],
    price: [
        {
            key: "price_empty",
            message: "Veuillez renseigner le prix",
            isValid: (v) => v !== ''
        }
    ],
    fee: [
        {
            key: "fee_empty",
            message: "Veuillez renseigner les frais (même 0)",
            isValid: (v) => v !== ''
        }
    ],
    created_at: [
        {
            key: "favorite_empty",
            message: "Veuillez renseigner la date de l'ordre",
            isValid: (v) => v !== ''
        }
    ],
    product_id: [
        {
            key: "product_empty",
            message: "Veuillez renseigner la paire concernée",
            isValid: (v) => v !== ''
        }
    ],
    currency: [
        {
            key: "currency_empty",
            message: "Veuillez renseigner l'actif cédé",
            isValid: (v) => v !== ''
        }
    ],
    thing: [
        {
            key: "thing_empty",
            message: "Veuillez renseigner l'objet de la cession",
            isValid: (v) => v !== ''
        }
    ]
};