import React from 'react';

export const PlusCircleIcon = () => {
    return (
        <svg className="bi bi-plus-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M8 3.5a.5.5 0 01.5.5v4a.5.5 0 01-.5.5H4a.5.5 0 010-1h3.5V4a.5.5 0 01.5-.5z"
                  clipRule="evenodd"/>
            <path fillRule="evenodd" d="M7.5 8a.5.5 0 01.5-.5h4a.5.5 0 010 1H8.5V12a.5.5 0 01-1 0V8z"
                  clipRule="evenodd"/>
            <path fillRule="evenodd" d="M8 15A7 7 0 108 1a7 7 0 000 14zm0 1A8 8 0 108 0a8 8 0 000 16z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const CheckCircleIcon = () => {
    return (
        <svg className="bi bi-check-circle" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M15.354 2.646a.5.5 0 010 .708l-7 7a.5.5 0 01-.708 0l-3-3a.5.5 0 11.708-.708L8 9.293l6.646-6.647a.5.5 0 01.708 0z"
                  clipRule="evenodd"/>
            <path fillRule="evenodd"
                  d="M8 2.5A5.5 5.5 0 1013.5 8a.5.5 0 011 0 6.5 6.5 0 11-3.25-5.63.5.5 0 11-.5.865A5.472 5.472 0 008 2.5z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const UpdateIcon = () => {
    return (
        <svg className="bi bi-pencil" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M11.293 1.293a1 1 0 011.414 0l2 2a1 1 0 010 1.414l-9 9a1 1 0 01-.39.242l-3 1a1 1 0 01-1.266-1.265l1-3a1 1 0 01.242-.391l9-9zM12 2l2 2-9 9-3 1 1-3 9-9z"
                  clipRule="evenodd"/>
            <path fillRule="evenodd"
                  d="M12.146 6.354l-2.5-2.5.708-.708 2.5 2.5-.707.708zM3 10v.5a.5.5 0 00.5.5H4v.5a.5.5 0 00.5.5H5v.5a.5.5 0 00.5.5H6v-1.5a.5.5 0 00-.5-.5H5v-.5a.5.5 0 00-.5-.5H3z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const DeleteIcon = () => {
    return (
        <svg className="bi bi-trash" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path
                d="M5.5 5.5A.5.5 0 016 6v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm2.5 0a.5.5 0 01.5.5v6a.5.5 0 01-1 0V6a.5.5 0 01.5-.5zm3 .5a.5.5 0 00-1 0v6a.5.5 0 001 0V6z"/>
            <path fillRule="evenodd"
                  d="M14.5 3a1 1 0 01-1 1H13v9a2 2 0 01-2 2H5a2 2 0 01-2-2V4h-.5a1 1 0 01-1-1V2a1 1 0 011-1H6a1 1 0 011-1h2a1 1 0 011 1h3.5a1 1 0 011 1v1zM4.118 4L4 4.059V13a1 1 0 001 1h6a1 1 0 001-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const ArrowBarUpIcon = () => {
    return (
        <svg className="bi bi-arrow-bar-up" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M11.354 5.854a.5.5 0 000-.708l-3-3a.5.5 0 00-.708 0l-3 3a.5.5 0 10.708.708L8 3.207l2.646 2.647a.5.5 0 00.708 0z"
                  clipRule="evenodd"/>
            <path fillRule="evenodd"
                  d="M8 10a.5.5 0 00.5-.5V3a.5.5 0 00-1 0v6.5a.5.5 0 00.5.5zm-4.8 1.6c0-.22.18-.4.4-.4h8.8a.4.4 0 010 .8H3.6a.4.4 0 01-.4-.4z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const ArchiveIcon = () => {
    return (
        <svg className="bi bi-archive" width="1em" height="1em" viewBox="0 0 16 16" fill="#fff"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M2 5v7.5c0 .864.642 1.5 1.357 1.5h9.286c.715 0 1.357-.636 1.357-1.5V5h1v7.5c0 1.345-1.021 2.5-2.357 2.5H3.357C2.021 15 1 13.845 1 12.5V5h1z"
                  clipRule="evenodd"/>
            <path fillRule="evenodd"
                  d="M5.5 7.5A.5.5 0 016 7h4a.5.5 0 010 1H6a.5.5 0 01-.5-.5zM15 2H1v2h14V2zM1 1a1 1 0 00-1 1v2a1 1 0 001 1h14a1 1 0 001-1V2a1 1 0 00-1-1H1z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const FilterIcon = () => {
    return (
        <svg className="bi bi-filter" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M6 10.5a.5.5 0 01.5-.5h3a.5.5 0 010 1h-3a.5.5 0 01-.5-.5zm-2-3a.5.5 0 01.5-.5h7a.5.5 0 010 1h-7a.5.5 0 01-.5-.5zm-2-3a.5.5 0 01.5-.5h11a.5.5 0 010 1h-11a.5.5 0 01-.5-.5z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const PrintIcon = () => {
    return (
        <svg width="16" height="16" shapeRendering="geometricPrecision" textRendering="geometricPrecision" imageRendering="optimizeQuality" fillRule="evenodd" clipRule="evenodd" viewBox="0 0 640 640" fill={"currentColor"}>
            <path fillRule="nonzero"
                  d="M120.84 176.116V25.536a2.735 2.735 0 0 1 2.74-2.74h301.067c.46 0 .886.106 1.263.295l10.052 4.146-1.051 2.539 1.05-2.54c.414.166.745.426 1.028.745l67.336 66.437 4.358 4.288-1.913 1.96 1.925-1.96c.543.543.815 1.252.815 1.96v75.45a2.742 2.742 0 0 1-2.74 2.74h-29.245a2.745 2.745 0 0 1-2.752-2.74v-35.858h-77.352a2.757 2.757 0 0 1-2.763-2.752c0-.071.012-.154.012-.237v-14.433h-.012V57.65H155.683v118.466a2.745 2.745 0 0 1-2.752 2.74h-29.35a2.735 2.735 0 0 1-2.74-2.74zm40.382 200.058h317.555c.225 0 .426.035.638.07 4.335.084 8.48.993 12.272 2.564a33.659 33.659 0 0 1 10.96 7.358 33.53 33.53 0 0 1 7.3 10.926 33.137 33.137 0 0 1 2.528 12.224c.059.2.094.425.094.638v48.154h68.529c.189 0 .39.024.578.047a23.44 23.44 0 0 0 8.54-1.842c2.94-1.229 5.598-3.048 7.866-5.292a24.553 24.553 0 0 0 5.291-7.866 23.459 23.459 0 0 0 1.831-8.575c-.023-.189-.059-.366-.059-.543V249.629c0-.213.036-.39.071-.59a23.74 23.74 0 0 0-1.843-8.54 24.502 24.502 0 0 0-5.29-7.855c-2.269-2.244-4.926-4.063-7.867-5.303a23.342 23.342 0 0 0-8.599-1.83 2.503 2.503 0 0 1-.52.047H58.904c-.19 0-.39-.012-.58-.06-2.987.072-5.87.721-8.539 1.843-2.94 1.24-5.598 3.06-7.866 5.303a24.502 24.502 0 0 0-5.291 7.855 23.635 23.635 0 0 0-1.831 8.598c.024.166.035.343.035.532v184.42c0 .189-.011.39-.047.567.071 3 .72 5.882 1.843 8.54a24.553 24.553 0 0 0 5.291 7.865c2.268 2.244 4.925 4.063 7.866 5.292a23.592 23.592 0 0 0 8.599 1.842c.165-.035.342-.047.52-.047h68.528v-48.142c0-.225.035-.45.07-.638a33.877 33.877 0 0 1 2.552-12.26 34.303 34.303 0 0 1 6.945-10.56c.106-.153.224-.295.354-.413a33.631 33.631 0 0 1 10.95-7.299 33.94 33.94 0 0 1 12.271-2.551c.213-.036.414-.071.638-.071h.012zM512.57 492.963v90.474c0 .224-.035.425-.094.637a32.978 32.978 0 0 1-2.551 12.272c-1.701 4.122-4.229 7.843-7.347 10.961a33.53 33.53 0 0 1-10.925 7.3 33.886 33.886 0 0 1-12.237 2.539c-.2.035-.413.059-.626.059H161.211c-.213 0-.425-.024-.626-.06a33.886 33.886 0 0 1-12.237-2.539 33.53 33.53 0 0 1-10.925-7.299c-3.118-3.118-5.646-6.839-7.346-10.96a32.977 32.977 0 0 1-2.552-12.273 2.315 2.315 0 0 1-.094-.637v-90.474H58.902c-.2 0-.413-.035-.625-.07a58.839 58.839 0 0 1-21.875-4.454 58.909 58.909 0 0 1-31.89-31.902c-2.8-6.744-4.37-14.126-4.453-21.862a2.827 2.827 0 0 1-.07-.626v-184.42c0-.224.023-.437.07-.626.083-7.748 1.654-15.142 4.453-21.886a59.11 59.11 0 0 1 12.768-19.11 58.66 58.66 0 0 1 19.122-12.768 58.613 58.613 0 0 1 21.875-4.465c.2-.047.401-.071.625-.071h522.195c.201 0 .414.024.626.07a58.661 58.661 0 0 1 21.875 4.466 58.66 58.66 0 0 1 19.122 12.767 59.11 59.11 0 0 1 12.768 19.11c2.8 6.745 4.37 14.139 4.453 21.887.047.189.07.402.07.626v184.408c0 .224-.023.425-.07.626-.083 7.748-1.654 15.13-4.453 21.874a58.96 58.96 0 0 1-12.768 19.134 59.035 59.035 0 0 1-19.122 12.768 58.839 58.839 0 0 1-21.875 4.453c-.2.036-.425.071-.626.071H512.57zm-46.571-387.56l-36.485-35.977v35.977h36.485zm11.705 305.626H162.298V582.35h315.405V411.029z"/>
        </svg>
    )
};

export const ChevronExpandIcon = () => {
    return (
        <svg className="bi bi-chevron-expand" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path
                fillRule="evenodd"
                d="M3.646 9.146a.5.5 0 01.708 0L8 12.793l3.646-3.647a.5.5 0 01.708.708l-4 4a.5.5 0 01-.708 0l-4-4a.5.5 0 010-.708zm0-2.292a.5.5 0 00.708 0L8 3.207l3.646 3.647a.5.5 0 00.708-.708l-4-4a.5.5 0 00-.708 0l-4 4a.5.5 0 000 .708z"
                clipRule="evenodd"
            />
        </svg>
    )
};

export const HamburgerIcon = () => {
    return (
        <svg className="bi bi-list" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M2.5 11.5A.5.5 0 013 11h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 7h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5zm0-4A.5.5 0 013 3h10a.5.5 0 010 1H3a.5.5 0 01-.5-.5z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const PencilSquareIcon = () => {
    return (
        <svg className="bi bi-pencil-square" width="1em" height="1em" viewBox="0 0 16 16"
             fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path
                d="M15.502 1.94a.5.5 0 010 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 01.707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 00-.121.196l-.805 2.414a.25.25 0 00.316.316l2.414-.805a.5.5 0 00.196-.12l6.813-6.814z"/>
            <path fillRule="evenodd"
                  d="M1 13.5A1.5 1.5 0 002.5 15h11a1.5 1.5 0 001.5-1.5v-6a.5.5 0 00-1 0v6a.5.5 0 01-.5.5h-11a.5.5 0 01-.5-.5v-11a.5.5 0 01.5-.5H9a.5.5 0 000-1H2.5A1.5 1.5 0 001 2.5v11z"
                  clipRule="evenodd"/>
        </svg>
    )
};

export const CalculatorIcon = () => {
    return (
        <svg className="bi bi-newspaper" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M0 2A1.5 1.5 0 0 1 1.5.5h11A1.5 1.5 0 0 1 14 2v12a1.5 1.5 0 0 1-1.5 1.5h-11A1.5 1.5 0 0 1 0 14V2zm1.5-.5A.5.5 0 0 0 1 2v12a.5.5 0 0 0 .5.5h11a.5.5 0 0 0 .5-.5V2a.5.5 0 0 0-.5-.5h-11z"/>
            <path
                d="M2 3h10v2H2V3zm0 3h4v3H2V6zm0 4h4v1H2v-1zm0 2h4v1H2v-1zm5-6h2v1H7V6zm3 0h2v1h-2V6zM7 8h2v1H7V8zm3 0h2v1h-2V8zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1zm-3 2h2v1H7v-1zm3 0h2v1h-2v-1z"/>
        </svg>
    )
};

export const RefreshIcon = () => {
    return (
        <svg className={`bi bi-arrow-repeat`} width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M2.854 7.146a.5.5 0 0 0-.708 0l-2 2a.5.5 0 1 0 .708.708L2.5 8.207l1.646 1.647a.5.5 0 0 0 .708-.708l-2-2zm13-1a.5.5 0 0 0-.708 0L13.5 7.793l-1.646-1.647a.5.5 0 0 0-.708.708l2 2a.5.5 0 0 0 .708 0l2-2a.5.5 0 0 0 0-.708z"/>
            <path fillRule="evenodd"
                  d="M8 3a4.995 4.995 0 0 0-4.192 2.273.5.5 0 0 1-.837-.546A6 6 0 0 1 14 8a.5.5 0 0 1-1.001 0 5 5 0 0 0-5-5zM2.5 7.5A.5.5 0 0 1 3 8a5 5 0 0 0 9.192 2.727.5.5 0 1 1 .837.546A6 6 0 0 1 2 8a.5.5 0 0 1 .501-.5z"/>
        </svg>
    )
};

export const CreditCardIcon = () => {
    return (
        <svg className="bi bi-credit-card" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M14 3H2a1 1 0 0 0-1 1v8a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4a1 1 0 0 0-1-1zM2 2a2 2 0 0 0-2 2v8a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V4a2 2 0 0 0-2-2H2z"/>
            <rect width="3" height="3" x="2" y="9" rx="1"/>
            <path d="M1 5h14v2H1z"/>
        </svg>
    )
};

export const ClipBoardIcon = () => {
    return (
        <svg className="bi bi-clipboard" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor"
             xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd"
                  d="M4 1.5H3a2 2 0 0 0-2 2V14a2 2 0 0 0 2 2h10a2 2 0 0 0 2-2V3.5a2 2 0 0 0-2-2h-1v1h1a1 1 0 0 1 1 1V14a1 1 0 0 1-1 1H3a1 1 0 0 1-1-1V3.5a1 1 0 0 1 1-1h1v-1z"/>
            <path fillRule="evenodd"
                  d="M9.5 1h-3a.5.5 0 0 0-.5.5v1a.5.5 0 0 0 .5.5h3a.5.5 0 0 0 .5-.5v-1a.5.5 0 0 0-.5-.5zm-3-1A1.5 1.5 0 0 0 5 1.5v1A1.5 1.5 0 0 0 6.5 4h3A1.5 1.5 0 0 0 11 2.5v-1A1.5 1.5 0 0 0 9.5 0h-3z"/>
        </svg>
    )
}