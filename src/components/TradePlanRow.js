import React from 'react';
import TradeButton from "./TradeButton";
import {getSymbol} from "../utils";

const TradePlanRow = ({actionable, data,updateOrder, fillOrder, deleteOrder, addAbove, lastKey, pair}) => {

    let curr1 = pair.split('-')[1]; //eur
    let curr2 = pair.split('-')[0]; //eth

    return(
        <tr className={"tp-row"}>
            <td className={`${data.type === "sell" ? "sell" : "buy"}`}>{`${data.type === "sell" ? "Vente" : "Achat"}`}</td>
            <td>{`${Math.round(data.quantite * 10) / 10}%`}</td>
            <td>{`${Math.round(data.tokens*100)/100}`}{`${data.type === "sell" ? getSymbol(curr2) : getSymbol(curr1)}`}</td>
            <td>{`${Math.round(data.prix*100) /100}`}{getSymbol(curr1)}</td>
            <td>{`${Math.round(data.recu*100)/100}`}{`${data.type === "sell" ? getSymbol(curr1) : getSymbol(curr2)}`}</td>
            <td>{`${Math.round(data.gains * 100) / 100}`}{`${data.type === "sell" ? getSymbol(curr1) : getSymbol(curr2)}`}</td>
            <td><TradeButton actionable={actionable} onUpdate={() => updateOrder("update", data.key)} onAddAbove={() => addAbove("above", data.key)} onFill={() => fillOrder(data.key)} onDelete={() => deleteOrder(data.key)} dataKey={data.key} statusDate={data.filledAt} status={data.orderStatus} lastOrder={data.key === lastKey}/></td>
        </tr>
    )
};

export default TradePlanRow;