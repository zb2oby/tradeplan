import React from 'react';

const ProgressBar = ({percentage}) => {


    return (
        <div className={"progressBar"}>
            <span className={"percentageInfo"}>{percentage}%</span>
            <div style={{width: `${percentage}%`}} className={"progressFiller"} />
        </div>
    )
};

export default ProgressBar;