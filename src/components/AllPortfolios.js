import React, {useState, useEffect} from 'react';
import {getAccount, getTransfersList} from "../service/apiCalls";
import {
    candlePromise,
    convertUnknownDateFormatToISO,
    getSymbol,
    isDefaultCurr,
    isFiat,
    promiseThrottle
} from "../utils";


const AllPortfolios = ({defaultCurr, className, pair, otherPrices}) => {


    let symbol = "";
    if (defaultCurr) {
        symbol = getSymbol(defaultCurr);
    }

    const [portfolioCurr1, setPortfolioCurr1] = useState(0.00);
    const [accounts, setAccounts] = useState([]);
    const [transfers, setTransfers] = useState([]);
    const [balance, setBalance] = useState((0.00));
    const [deposits, setDeposits] = useState(0.00);
    const [totalGain, setTotalGain] = useState(0.00);



    useEffect(() => {
        getTransfersList().then((tr) => setTransfers(tr));
        getAccount("AllPortfolios").then(ac => {setAccounts(ac);});
    }, []);


    useEffect(() => {
        //RECUPERATION BALANCE TOTALE
        //pour chaque compte, récupere la balance et convertir en valeur curr1
        let bal = 0.00;
        accounts.forEach((a) => {
            if (isFiat(a.currency)) { //cas d'une monnaie FIAT
                setPortfolioCurr1(parseFloat(a.balance));
                bal += parseFloat(a.balance)
            } else {
                let balance = parseFloat(a.balance);
                if (otherPrices) {
                    let currency = otherPrices.find((op) => op.pair.split("-")[0] === a.currency);
                    if (currency) {
                        let otherCurr2 = currency.pair.split('-')[1];

                        let price = 0.00;
                        if (isDefaultCurr(otherCurr2)) {
                            price = parseFloat(currency.price);
                            bal += balance * price;
                        } else {
                            //sinon convertir le prix du non fiat en defaultFiat
                            //aller chercher le prix actuel dans otherPrices de la paire [otherCurr2-defaultfiat]
                            let fiatable = otherPrices.find((op) => op.pair === `${otherCurr2}-${defaultCurr}`);
                            if (fiatable) {
                                price = parseFloat(fiatable.price * currency.price);
                                bal += balance * price;
                            }
                        }



                    }
                }
            }
        });
        setBalance(bal)

    }, [otherPrices]);


    useEffect(() => {

        let promises = transfers.map(async (r) => {
            let account = accounts.find((a) => a.id === r.account_id);
            if (account) {
                let pairToSearch = account.currency + "-" + defaultCurr;
                if (!isFiat(account.currency)) { //si le dépot est en jetons
                    let dateStart = convertUnknownDateFormatToISO(r.completed_at)
                    return promiseThrottle.add(candlePromise.bind(this, dateStart, pairToSearch, {object: r, field: "amount"}, "AllPortfolios"))
                }
            }
            return new Promise(resolve => resolve(r));
        });

        Promise.all(promises).then((res) =>  {
            let launch = 0.00;
            if (res.length > 0 ) {
                res.forEach((transfer) => {
                    let amount = parseFloat(transfer.amount);
                    if (transfer.type === "deposit") {
                        launch += amount;
                    } else {
                        launch -= amount;
                    }
                });
            }


            setDeposits(launch);
        }).catch(err => console.log(err));




    }, [transfers, accounts, defaultCurr]);


    useEffect(() => {
        if (balance && deposits) {
            let gain = balance - deposits
            setTotalGain(gain);
        }
    }, [balance, deposits])


    return (
        <div className={`all-portfolio-wrapper ${className ? className : ""}`} style={{color: "#fff"}}>
            <div className={"all-portfolio-item"}><span className={"state-label"}>Disponible</span>{Math.round(portfolioCurr1 * 100) / 100}{symbol}</div>
            <div className={"all-portfolio-item"}><span className={"state-label"}>Balance</span>{Math.round((balance) * 100)/100}{symbol}</div>
            <div className={"all-portfolio-item"}><span className={"state-label"}>Valeur dépots</span> {Math.round(deposits *100) / 100}{symbol}</div>
            <div className={"all-portfolio-item"}>
                <span className={"state-label"}>Gains/Pertes</span>
                <span style={{color: `${totalGain < 0 ? "red" : "green"}`}}>{Math.round(totalGain *100) / 100}{symbol}</span>
            </div>
        </div>
    )
};

export default AllPortfolios;