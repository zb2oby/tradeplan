import React from 'react';
import logo from '../ETH-EUR.png'


const Signin = () => {


    return (
        <div className="app-container">
            <div className={"signin-container"}>
                <img src={logo} className="app-logo" alt="logo" />
                <p>StopLoss</p>
                <div id="loginButton">Login with Google</div>
            </div>
        </div>
    )

};

export default Signin;