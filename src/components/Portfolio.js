import React, {useEffect, useRef, useState} from 'react';
import {
    getAccount,
    getCoinbaseLedger,
    getCurrentPrice,
    getFillHistory2,
    readAllInit
} from "../service/apiCalls";
import {TitleComponent} from "./TitleComponent";
import WidgetItem from "./WidgetItem";
import PortfolioState from "./widgets/PortfolioState";
import History from "./widgets/History";
import TradePlan from "./widgets/TradePlan";
import {
    candlePromise,
    checkUserAgentVisibility,
    detectMobileBrowser, initOrdersFromMatches, initPositionsFromOrders, isSupportedCurr, promiseThrottle
} from "../utils";
import Alert from "./Alert";
import {supportedPairs} from "./Constants";
import Loading from "./Loading";



const Portfolio = ({pair, deferSocket, otherPrices, setOtherPrices}) => {

    const [orders, setOrders] = useState([]);
    const [positions, setPositions] = useState([]);
    const [currentPrice, setCurrentPrice] = useState('');
    const [currentPriceTime, setCurrentPriceTime] = useState('');
    const [titlePrice, setTitlePrice] = useState('');
    const [webSocketClient, setWebSocketClient] = useState(null);
    const [error, setError] = useState(null);
    const [historyRef, setHistoryRef] = useState(null);
    const [titleComp, setTitleComp] = useState("Tous");
    const [isLoading, setIsLoading] = useState(false);
    const [isInit, setInit] = useState(false);
    const [isCalculated, setIsCalculated] = useState(false);

    //on met à jour les données si on a une connection
    if (webSocketClient !== null) {
        webSocketClient.onmessage = (message) => {
            const data = JSON.parse(message.data)
            /*console.log("WEBSOCKET", data);*/
            if(data.type === "error") {
                console.log('Socket error.', JSON.parse(data.reason));
                console.log('Closing socket')
                webSocketClient.close();
                /*setCurrentPrice('');
                setPositions([]);
                setOrders([]);*/
                setError(JSON.parse(data.reason).message)
            }
            if (data.type === "ticker") {

                //Enregistrement du prix courant sur la paire demandée
                if (data.product_id === pair) {
                    setCurrentPrice(data.price);
                    setCurrentPriceTime(data.time);
                    setTitlePrice(data.price);
                }

                //sauvegarde du dernier prix par paire pour mise a jour prix directement au changement de paire selectionnée sans attendre un nouveau message
                let updateOtherPrices = otherPrices.map((op) => {
                    if (op.pair === data.product_id) {
                        return {...op, price: data.price}
                    }
                    return op;
                });
                setOtherPrices(updateOtherPrices);


            }
        };
        webSocketClient.onclose = (e) => {
            console.log('Socket is closed.', e.reason, e.code, "isClean=" + e.wasClean);
            if (!e.wasClean && (e.code === 1006 || e.code === 1005 || e.code === 1001) && navigator.onLine) {
                console.log('Attempt to reconnect in 1 sec');
                setTimeout(() => {
                    getCurrentPrice(supportedPairs).then((res) => setWebSocketClient(res))
                }, 1000);
            }
        }
    }


    const initWebSocket = () => {
        setIsLoading(true)
        getCurrentPrice(supportedPairs).then((res) => {
            console.log("Openning socket");
            setWebSocketClient(res);
            deferSocket(res);
            setIsLoading(false)
        });
    };


    const initHistory = () => {
        setInit(true);

        const historyPromises = supportedPairs.map((sp) => {
            return getFillHistory2(sp);
        });

        Promise.all(historyPromises).then((res) => {
            if (res) {
                let allHistory = res.flat();

                readAllInit().then(async (init) => {

                    let ord = [];
                    init.forEach((ini) => {
                        ini.data.orders.forEach((o) => ord.push(o))
                    });

                    if (init !== undefined) {
                        setHistoryRef(init);

                    } else {
                        setHistoryRef(null);
                    }

                    //ajout des ordres manuellements enregistrés avec les ordres récuperé de l'api coinbase
                    let historyWithInit = ord.concat(allHistory);

                    //traitement des ordres "buy" possedant un "match" negatif (reduction du montant des ordres. ainsi on evite de garder des ordre d'achat complet dans le panier global alors u'ils ont servi pour des achat autres
                    let accounts = await getAccount('portfolio')
                    //console.log("HISTINIT-ACC", accounts);

                    let filteredAccounts = accounts.filter((a) => isSupportedCurr(a.currency));
                    //on recupere les registres des comptes supportés
                    const ledgers = (accounts) => {
                        const s = (account) => {
                            return new Promise(async resolve => {
                                getCoinbaseLedger(account.id).then((res) => {
                                    resolve( {currency: account.currency, ledger: res})
                                });
                            });
                        };

                        const promises = accounts.map(async (account) => {
                            return promiseThrottle.add(s.bind(null, account));
                        });
                        return Promise.all(promises);
                    };

                    //avec ces registres on verifie les matchs necessitants un retraitement
                    ledgers(filteredAccounts).then((leds) => {
                        leds.forEach((led, index) => {
                            //console.log("HISTINIT-LED", led);
                            let matches = led.ledger.filter((l) => l.amount < 0 && l.type === "match");
                            let fees = led.ledger.filter((l) => l.type === "fee");
                            if (matches.length > 0) {
                                let product = led.currency;
                                //Réduit les ordres des montants correspindant à des match negatif dont l'ordre est un buy == conversion d'une paire à une autre et non à un trade pur (match negatif don ordre est un sell).
                                historyWithInit = initOrdersFromMatches(product, historyWithInit, matches, fees)
                            }

                            if (index === leds.length -1 ) {
                                //console.log("HISTINIT-HIST", historyWithInit);
                                setOrders(historyWithInit)

                            }

                        });
                        setInit(false);
                    });
                });
            }

        }).catch((err) => setError(err.message));
    };


    useEffect(() => {
        let initOtherPrices = [];
        supportedPairs.forEach((sp) => {
            initOtherPrices.push({pair: sp, price: 0})
        });
        setOtherPrices(initOtherPrices);
        initWebSocket();
        initHistory();
    }, []);


    useEffect(() => {

        if (pair) {

            if (orders.length > 0) {

                let posOrders = orders.filter((i) => i.product_id === pair);

                if (posOrders.length > 0) {
                    let currentPositions = initPositionsFromOrders(posOrders);
                    console.log("POSITIONS INIT", currentPositions);

                    setPositions(currentPositions)
                } else {
                    setPositions([])
                }

            } else {
                setPositions([]);
            }
        }

    }, [pair, orders]);


    useEffect(() => {
        //au changement de paire aller retrouver les frames (message du websocket) et choper le dernier existant pour la paire concernée afinde mettre à jour le current price
        if (otherPrices.length > 0 && otherPrices.some((f) => f.pair === pair)) {
            let lastFrame = otherPrices.find((f) => f.pair === pair);
            setCurrentPrice(lastFrame.price);
            setTitlePrice(lastFrame.price);
        }

    }, [pair]);

    const getState = () => {
        if (document.visibilityState === 'hidden') {
            return 'hidden';
        }
        if (document.hasFocus()) {
            return 'active';
        }
        return 'passive';
    };


    const handleConnectionChange = (event) => {

        setError(null);

        if(event.type === "offline"){
            console.log("You lost connection.");
            if (webSocketClient !== null) {
                console.log('Closing socket');
                webSocketClient.close();
            }
            setWebSocketClient(null);
            deferSocket(null);
        }
        if(event.type === "online"){
            console.log("You are now back online.");
            if (webSocketClient != null) {
                webSocketClient.close();
            }
            setTimeout(() => {
                initWebSocket();

                if (!isCalculated) {
                    initHistory();
                }

            }, 1500)
        }

    };


    const handleBrowserState = (event) => {


        console.log("wasDiscarded", document.wasDiscarded);
        console.log("STATE", getState());

        setError(null);

        let hidden = checkUserAgentVisibility().hidden;

        if (getState() === hidden) {
            if (webSocketClient !== null) {
                console.log(event.type, 'hiding tab');
                console.log("Closing socket");
                webSocketClient.close();
                setWebSocketClient(null);
                deferSocket(null);
            }

        } else if (getState() === "active" || event.type === "resume") {
            if (webSocketClient === null) {
                console.log(event.type, 'Showing tab');
                if (webSocketClient !== null) {
                    webSocketClient.close();
                }
                setTimeout(() => {
                    initWebSocket();

                    if (!isCalculated) {
                        initHistory();
                    }

                }, 1500)
            }
        }
    };



    useEffect(() => {


        console.log("adding event listeners")
        window.addEventListener('online', handleConnectionChange, false);
        window.addEventListener('offline', handleConnectionChange, false);

        let visibilityChange = checkUserAgentVisibility().visibilityChange;

        if (detectMobileBrowser()) {
            document.addEventListener(visibilityChange, handleBrowserState, false);
            window.addEventListener('blur', handleBrowserState, false);
            window.addEventListener('focus', handleBrowserState, false);

            document.addEventListener('freeze', (e) => console.log("page frozen"), false);
            document.addEventListener('resume', handleBrowserState, false);
        }

        return () => {
            console.log("removing previous event listeners")
            window.removeEventListener('online', handleConnectionChange, false);
            window.removeEventListener('offline', handleConnectionChange, false);
            document.removeEventListener(visibilityChange, handleBrowserState, false);
            window.removeEventListener('blur', handleBrowserState, false);
            window.removeEventListener('focus', handleBrowserState, false);
            document.removeEventListener('resume', handleBrowserState, false);
            document.removeEventListener('freeze', null, false);
        }

    }, [webSocketClient]);


    const onChangeHistory = (orders) => {
        setOrders(orders);
        let posOrders = orders.filter((i) => i.product_id === pair);
        let currentPositions = initPositionsFromOrders(posOrders);
        console.log("POSITIONS CHANGED", currentPositions);
        if (currentPositions) {
            setPositions(currentPositions)
        } else {
            setPositions([]);
        }
    };

    const onFilterChange= (filters) => {

        let titleSide = "";
        let titleProduct = "";
        let titleDate = "";
        filters.forEach((f) => {
            switch (f.field) {
                case "side":
                    titleSide = `${f.value === "sell" ? "Ventes" : "Achats"}`;
                    break;
                case "product_id":
                    titleProduct = ` - ${f.value}`;
                    break;
                case "created_at":
                    titleDate = ` - ${f.value}`;
                    break;
                default:
                    break;

            }
        });

        let titleC = `${titleSide}${titleProduct}${titleDate}`;
        if (titleC === "") {
            titleC = "Tous"
        }
        setTitleComp(titleC);
    };

    const componentRef = useRef();

    return (
        pair &&
            <>
                {error  !== null && <Alert className={"error"} reason={"Une erreur est survenue"} message={error} type={"warning"}/>}
                {isLoading || isInit && <Loading above={true}/>}

                <div ref={componentRef} className={"d-flex flex-row justify-content-around flex-wrap"}>
                    <WidgetItem titleComp={titleComp} collapse={true} collpaseId={"bt-collapse"} title={"Historique"} >
                        <History callBackCalculated={setIsCalculated} initHistory={initHistory} onFilterChange={onFilterChange} historyRef={historyRef} onChange={onChangeHistory} printRef={componentRef} orders={orders} styled={true} typed={true} pair={pair} printTitle={titleComp}/>
                    </WidgetItem>
                </div>

                <div className={"d-flex flex-column flex-xl-row"}>
                    <TitleComponent title={`€${titlePrice} - ${pair}`} icon={pair}/>

                    <WidgetItem className={"mr-lg-4"} title={`Etat des entrées ${pair}`} >
                        <PortfolioState priceTime={currentPriceTime} currentPrice={currentPrice} positions={positions} pair={pair}/>
                    </WidgetItem>

                    <WidgetItem title={`Plan de trading ${pair} (projections et suivi)`} >
                        <TradePlan positions={positions} pair={pair}/>
                    </WidgetItem>

                </div>


            </>

    )
}

export default Portfolio;