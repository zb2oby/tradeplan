import React, {useState, useEffect} from 'react';
import {supportedPairs} from "./Constants";

const ComputeTaxModal = ({onSubmit}) => {

    const [year, setYear] = useState('');

    return (
        <div className="modal fade" id={`compute-tax`} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h6 className="modal-title" id="exampleModalLongTitle">Pour quelle année souhaitez vous afficher le calcul ?</h6>
                    </div>
                    <div className="modal-body">
                        <div className={"form-group"}>
                            <label htmlFor="">Année</label>
                            <input value={year} onChange={(e) => setYear(e.target.value)} type="text" required className={"form-control"}/>
                        </div>
                        <div className={"small text-warning"}>Lancez directement le calcul si vous souhaitez afficher pour l'ensemble de l'historique.</div>
                    </div>
                    <div className="modal-footer">
                        <button type="button"  className="btn btn-primary" data-dismiss="modal" onClick={() => onSubmit(year)}>Lancer le calcul</button>
                        <button type="button"  className="btn btn-primary" data-dismiss="modal">Annuler</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default ComputeTaxModal;