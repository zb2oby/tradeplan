import React, {useState, useEffect} from 'react';
import {
    candlePromise,
    containFiat, isFiat,
    promiseThrottle
} from "../../utils";
import OrderTable from "../OrderTable";
import AddHistoryModal from "../AddHistoryModal";
import {
    createHistory,
    createHistoryCollectionIfNotExists,
    updateHistory
} from "../../service/apiCalls";
import Loading from "../Loading";
import {supportedPairs} from "../Constants";
import {CalculatorIcon, FilterIcon, PlusCircleIcon, RefreshIcon} from "../../icons/Icons";
import ComputeTaxModal from "../ComputeTaxModal";
import ProgressBar from "../ProgressBar";

//http://www.assemblee-nationale.fr/dyn/15/amendements/1255C/AN/2523

// Le prix total d’acquisition du portefeuille de crypto-actifs est égal à la somme des prix effectivement acquittés réalisées avant la cession
// La valeur globale du portefeuille de crypto-actifs est égale à la somme des valeurs, évaluées au moment de la cession
//Le prix de cession à retenir est le prix réel perçu ou la valeur de la contrepartie obtenue par le cédant
//Le prix de cession est réduit, sur justificatifs, des frais supportés par le cédant à l’occasion de cette cession.

//plus/moins value = prix de vente - (cout d'achat total * prix de vente / valeur totale au moment de la vente)

//achat 1 eth a 50 + 1 eth a 200
//j'en vend 1 à 100
//100 - (250 * 100 / 200) = 100 - 125 = -25

//la plus/moins value au moment de la vente est donc de -25. même si dans l'absolue j'ai "gagné" 50 (puisque j'ai vendu 1 eth à 100 acheté 50) en fait je n'ai que "combler" le manque à gagner au moment de la vente



const History = ({callBackCalculated, initHistory, onFilterChange, historyRef, orders, styled, typed, pair, printRef, printTitle, onChange}) => {

    const [toUpdate, setToUpdate] = useState('');
    const [isLoading, setIsLoading] = useState(false);
    const [historyLoading, setHistoryLoading] = useState(false);
    const [localOrders, setLocalOrders] = useState([]);
    const [orderElts, setOrderElts] = useState([]);
    const [totalPL, setTotalPL] = useState([]);
    const [totalFee, settotalFee] = useState([]);
    const [filterVisible, setFilterVisible] = useState(false);
    const [filterValue, setFilterValue] = useState([]);
    const [isCalculated, setIsCalculated] = useState(false);
    const [progressCalcul, setProgressCalcul] = useState(0);
    const [modalType, setModalType] = useState('');

    let curr1 = pair.split('-')[1]; //eur
    let curr2 = pair.split('-')[0]; //eth



    useEffect(() => {
        setOrderElts(orders);
    }, [orders]);

    /**
     * Recupere la liste des ordres et les modifies avec la valeur des autres paire existantes à date afin de lancer le calcul d'imposition
     */
    const handleTaxCalculation = (year) => {

        setHistoryLoading(true);

        const s = (otherCurrencies, order) => {
            return new Promise(async resolve => {
                nestedPromise(otherCurrencies, order).then((res) => {
                    //console.log("OTHER", res);
                    resolve( {...order, otherPrices: res})
                });

            });
        };

        const ordersWithOtherPrices = orders.map((order) => {
            if (order.side === "sell") {
                let otherCurrencies = [];
                orders.forEach((o) => {
                    if (o.product_id !== order.product_id && o.created_at < order.created_at && !otherCurrencies.some((oc) => o.product_id === oc.product_id)) {
                        otherCurrencies.push({product_id: o.product_id, price: 1.00}); //price == 1 car le candlePromise effectue une multiplication du champs output par le prix candle trouvé
                    }
                });

                //pour chacune des paires trouvées recuperation du prix à la date de l'ordre
                if (otherCurrencies.length > 0) {
                    return promiseThrottle.add(s.bind(null, otherCurrencies, order))
                }
                return new Promise(resolve => resolve(order));
            }
            return new Promise(resolve => resolve(order))

        });


        //const countSellOrders = orders.filter((o) => o.side === "sell").length;

        let countPromises = 0;
        for (const p of ordersWithOtherPrices) {
            p.then(()=> {
                countPromises ++;
                let progress = (countPromises * 100) / ordersWithOtherPrices.length
                //console.log("PROGRESS", progress, "NUMBER", countPromises, ordersWithOtherPrices.length)
                setProgressCalcul( Math.round(progress));
            });
        }
        if (orders.length > 0) {
            Promise.all(ordersWithOtherPrices).then((res) => {
                //console.log("PRICES", res);
                setLocalOrders(res);
                let filters = [{field: 'created_at', value: year}, {field: 'side', value: "sell"}]
                setFilterValue(filters);
                onFilterChange(filters);
                setIsCalculated(true);
                callBackCalculated(true);
                setHistoryLoading(false)
                console.log("history is ready")

            }).catch((err) => console.log(err))

        }
    };

    const nestedPromise = (otherCurrencies, order) => {

        const promises = otherCurrencies.map( async (t) => {
            let pairToSearch = t.product_id;
            let dateStart = order.created_at;
            return candlePromise(dateStart, pairToSearch, {object: t, field: "price"}, "History");
        });

        return Promise.all(promises);

    };

    const deleteOrder = (tradeId, tradePair) => {

        let documentData = {
            pair: tradePair,
            orders: orders.filter((o) => o.user_id === undefined && o.trade_id !== tradeId && o.product_id === tradePair)
        };

        let ref = historyRef.find((r) => r.data.pair === tradePair);
        setIsLoading(true);
        updateHistory(ref.ref["@ref"].id, documentData, pair).then((res) => {
            //mise à jour liste d'ordre et positions en cours
            let updated = orders.filter((o) => o.trade_id !== tradeId);
            onChange(updated);
            setIsLoading(false);
        });


    };

    const saveOrder = (data) => {

        let local = orders.map((o) => o);

        if (toUpdate !== "") {
            //cas d'un modification
            local.map((lo) => {
                if (lo.trade_id === toUpdate.trade_id) {
                    lo.price = data.price;
                    lo.side = data.side;
                    lo.size = data.size;
                    lo.fee = data.fee;
                    lo.product_id = data.product_id;
                    lo.created_at = data.created_at;
                    if (data.sellType && data.sellType === "external") {
                        lo.thing = data.thing;
                        lo.sellType = data.sellType;
                        if (data.description) {
                            lo.description = data.description
                        }
                    }
                    return lo;
                }
                return lo;
            });


        } else {
            //cas d'une création
            local.push(data);

            local.sort((a, b) => {
                let dateA = new Date(a.created_at);
                let dateB = new Date(b.created_at);
                return dateA - dateB
            });

            //creation d'un id pour l'odre passé
            for(let i = 0; i < local.length; i++)
            {
                if (local[i].trade_id === 0) {
                    let previousId = 0;
                    if (local[i-1] !== undefined) {
                        previousId = local[i-1].trade_id;
                    }
                    let nextId
                    if (local[i+1] !== undefined) {
                        nextId = local[i+1].trade_id;
                        local[i]["trade_id"] = Math.floor(Math.random() * (nextId + 1 - previousId + 1) + previousId + 1 );
                    } else {
                        local[i]["trade_id"] = previousId + 100;
                    }

                }


            }
        }

        //on prepare le document à renvoyer en enlevant les ordres provennat de l'api coinbase
        let documentData = {
            pair: data.product_id,
            orders: local.filter((o) => o.user_id === undefined && o.product_id === data.product_id)
        };

        //enregistrement bdd
        setIsLoading(true);
        //test existance document

        //si existe pas creation
        let ref = historyRef.find((r) => r.data.pair === data.product_id);
        //console.log(ref, data.product_id, documentData)

        if (ref === undefined) {
            //console.log("CREATE HISTORY")
            createHistoryCollectionIfNotExists(data.product_id).then((res) => {
                createHistory(documentData).then((res)=> {
                    //mise à jour liste d'ordre et positions en cours
                    onChange(local);
                    setIsLoading(false);
                    initHistory();
                    setModalType('')
                });
            })
        } else { //sinon update

            updateHistory(ref.ref["@ref"].id, documentData, data.product_id).then((res) => {
                //mise à jour liste d'ordre et positions en cours
                onChange(local);
                setIsLoading(false);
                setModalType('')
            })
        }



    };

    /**Effectue les calcul d'imposition et rempli les champs necessaires à l'affichage des + value imposables*/
    useEffect(() => {

        if (isCalculated) { //evite de passer au changement de filtre si on est pas dans le contexte "calculé"
            let temporaryGrouped = [];
            //detecter dans la liste d'ordre les différentes paires et créer les objet temporaire si inexistants
            localOrders.forEach((lo) => {
                //liste d'objet temporaire groupBy productId basé sur la liste des ordres:
                if (!temporaryGrouped.find((g) => g.pair === lo.product_id)) {
                    temporaryGrouped.push({
                        pair: lo.product_id,
                        nombreToken: 0.00,
                        //buyOrders: []
                    })
                }


            });

            let prixAcquisition = 0.00;
            let totalGain = 0.00;
            let partCapitalInitDeduite = 0.00;
            let totalFee = 0.00;
            //trier tous les ordre par date
            const orderElts = []
            localOrders.sort((a, b) => {
                let dateA = new Date(a.created_at);
                let dateB = new Date(b.created_at);
                return dateA - dateB
            });

            for (let i = 0; i < localOrders.length; i++) {

                //console.log("CONTAIN", localOrders[i].product_id, containFiat(localOrders[i].product_id))

                //

                if (containFiat(localOrders[i].product_id)
                    && ( !localOrders[i].sellType
                        || localOrders[i].sellType && (localOrders[i].sellType === "external" && (localOrders[i].thing === "other" || isFiat(localOrders[i].thing) ) )
                    )
                ) { //on effectue le calcul que pour les paires contenants des fiat (reglmentation Français) Ou pour l'achat de bien ou service avec la crypto



                    //l'ordre en cours de traitement
                    let size = parseFloat(localOrders[i].size);
                    let price = parseFloat(localOrders[i].price);
                    let fee = parseFloat(localOrders[i].fee);

                    let gain = 0.00;

                    //recuperation de l'objet temporaire pour cette paire
                    let currentCurrency = temporaryGrouped.filter((g) => g.pair === localOrders[i].product_id)[0];


                    //si buy
                    //remplissage objet temporaire (nombre)
                    //mise à jour prix acquisition
                    if (localOrders[i].side === "buy") {
                        //totalSize += size;
                        currentCurrency.nombreToken = currentCurrency.nombreToken + size;
                        //temp.buyOrders.push(localOrders[i]);

                        prixAcquisition = prixAcquisition - partCapitalInitDeduite + (size * price) + fee
                        partCapitalInitDeduite = 0.00;

                    }

                    //si sell
                    //mettre a jour valeurGlobale avec la paire en cours de traitement
                    //verifier presence nombreToken Autres monnaie
                    //si existe recuperer prix à date de l'ordre en cours de traitement de ces monnaie (getCandles)
                    //mettre à jour valeurGlobale avec les autres monnaies
                    //calculer les gains
                    //remplacement objet temporairep avec le nouveau
                    if (localOrders[i].side === "sell") {

                        let valeurGlobale = 0.00;
                        let otherCurrencies = temporaryGrouped.filter((t) => t.pair !== localOrders[i].product_id);

                        if (otherCurrencies.length > 0 && otherCurrencies.some((c) => c.nombreToken !== 0)) {
                            otherCurrencies.forEach(async (t) => {
                                //recup prix à date
                                let buyPrice = localOrders[i].otherPrices.find((op) => op.product_id === t.pair);
                                if (buyPrice) {
                                    valeurGlobale += (parseFloat(t.nombreToken) * buyPrice.price);
                                }
                            })

                        }

                        let prixVente = (size * price);
                        let prixNetVente = prixVente - fee;
                        valeurGlobale += currentCurrency.nombreToken * price;

                        let fraction = prixVente / valeurGlobale;
                        let prixNetAcquisition = prixAcquisition - partCapitalInitDeduite

                        gain = prixVente - (prixNetAcquisition * fraction);

                        //on sauvegarde les données pour le tour suivant
                        //total des fractions de capital
                        partCapitalInitDeduite += (fraction * prixNetAcquisition)
                        currentCurrency.nombreToken = currentCurrency.nombreToken - size;


                        localOrders[i] = {
                            ...localOrders[i],
                            globalValue: valeurGlobale,
                            sellPrice: prixVente,
                            netSellPrice: prixNetVente,
                            buyCost: prixAcquisition,
                            netBuyCost: prixNetAcquisition,
                            quotaInit: partCapitalInitDeduite
                        };

                        //Calcul du total gain en fonction de la date séléectionnée ou non
                        let yearRegex = /[0-9]{4}/
                        if (filterValue.some((f) => f.value !== "" && f.field === "created_at" && f.value.match(yearRegex))) {
                            let valueToApply = filterValue.find((f) => (f.field === "created_at")).value;
                            let dateSplited = localOrders[i].created_at.split("T")[0];
                            let yearSplited = dateSplited.split("-")[0];
                            if (yearSplited === valueToApply) {
                                totalGain += gain;
                                totalFee += fee;
                            }
                        } else {
                            totalGain += gain;
                            totalFee += fee;
                        }

                        otherCurrencies.push(currentCurrency);
                        temporaryGrouped = otherCurrencies

                    }

                    orderElts.push({...localOrders[i], gain: gain});
                } else {
                    orderElts.push({...localOrders[i]});
                }

            }


            //console.log("END", totalGain)
            setTotalPL(totalGain);
            setOrderElts(orderElts);
            settotalFee(totalFee);
        }


    }, [localOrders, filterValue, isCalculated]);


    const onUpdate = (tradeId, type) => {
        setToUpdate(tradeId);
        setModalType(type)
    };

    const onFilter = (field, value) => {

        let filters = filterValue.map((f) => f); //to clone in new object
        if (filterValue.some((f) => f.field === field)) {
            filters = filters.map((fil) => {
                if (fil.field === field) {
                    return {...fil, value: value}
                }
                return fil;
            });
        }else {
           filters.push({field: field, value: value})
        }
        setFilterValue(filters);
        onFilterChange(filters);


    };

    const handleRefresh = () => {
        initHistory();
        setFilterValue([]);
        onFilterChange([]);
        setIsCalculated(false);
    };

    const onReset = (field) => {
        let filters = filterValue.map((f) => f);
        let filtered = filters.filter((f) => f.field !== field);
        setFilterValue(filtered);
        onFilterChange(filtered);
    };


    let filtered = orderElts.sort((a, b) => {
        let dateA = new Date(a.created_at);
        let dateB = new Date(b.created_at);
        return dateB - dateA
    });
    if (filterValue.some((f) => f.value !== "")) {
        filterValue.forEach((fill) => {
            let yearRegex = /[0-9]{4}/
            if (fill.field === "created_at") {
                if (fill.value.match(yearRegex)) {
                    filtered = filtered.filter((f) => {
                        let dateSplited = f.created_at.split("T")[0];
                        let yearSplited = dateSplited.split("-")[0];
                        return yearSplited === fill.value
                    });
                }

            } else  if (fill.value !== "" && (!fill.type || (fill.type && fill.type !== "inverse"))) {
                filtered = filtered.filter((f) => f[fill.field] === fill.value);
            } else if (fill.value !== "" && ((fill.type && fill.type === "inverse"))) {
                filtered = filtered.filter((f) => f[fill.field] !== fill.value);
            }
        })
    }


    if (isCalculated) {
        filtered = filtered.filter((f) => {
            return !f.sellType || (f.thing === 'other' || isFiat(f.thing))
        })
    }



    const headsAfterCompute = [
        {id: "side", label: "Type"},
        {id: "created_at", label: "Date"},
        {id: "product_id", label: "Paire"},
        {id: "size", label: `Taille`},
        {id: "price", label: `Prix`},
        {id: "globalValue", label: `Valeur Globale`},
        {id: "sellPrice", label: `PV`},
        {id: "fee", label: `Frais`},
        {id: "netSellPrice", label: `PV Net`},
        {id: "buyCost", label: `PA`},
        {id: "quotaInit", label: `Fraction`},
        {id: "netBuyCost", label: `PA net`},
        {id: "gain", label: `+/- value`}
    ];

    const initHeads = [
        {id: "side", label: "Type"},
        {id: "created_at", label: "Date"},
        {id: "product_id", label: "Paire"},
        {id: "size", label: `Taille`},
        {id: "price", label: `Prix`},
        {id: "fee", label: `Frais`}
    ];

    let heads = isCalculated ? headsAfterCompute : initHeads;

    const optionsCurrency = supportedPairs.map((p,i) => <option key={i} value={p}>{p}</option>)

    const totalSection = isCalculated ? (
                <span className={"total-fisc"} style={{float: "left"}} >
                    <div>
                        {`TOTAL +/- value imposable : ${Math.round(totalPL*100)/100} ${curr1}`}
                    </div>
                    <div>
                        {`TOTAL Frais : ${Math.round(totalFee*100)/100} ${curr1}`}
                    </div>
                </span>
    ) : null;

    return (
        <>
            {isLoading &&
                <Loading above={true}/>
            }

            {filterVisible &&
            <div className={"filter"}>
                <div className={"filter-item"}>
                    <label htmlFor="">Type</label>
                    <select value={filterValue.some(f => f.field === "side") ? filterValue.find(f => f.field === "side").value : ""} className={"form-control history-filter"} onChange={(e) => onFilter("side", e.target.value)} name="" id="">
                        <option value={""}>Tous</option>
                        <option value="sell">Ventes</option>
                        <option value="buy">Achats</option>
                    </select>
                    <span title={"Réinitialiser"} onClick={(e) => {e.stopPropagation(); onReset("side");}} className={`filters-reset ${filterValue.some(f => f.field === "side" && f.value !== "") ? "filters-filled" : ""}`}>X</span>
                </div>
                <div className={"filter-item"}>
                    <label htmlFor="">Année</label>
                    <input placeholder={"Année"} min="2000" max="2099" value={filterValue.some(f => f.field === "created_at") ? filterValue.find(f => f.field === "created_at").value : ""} onChange={(e) => onFilter("created_at", e.target.value)} type="number" className={"form-control history-filter"}/>
                    <span title={"Réinitialiser"} onClick={(e) => {e.stopPropagation(); onReset('created_at');}} className={`filters-reset ${filterValue.some(f => f.field === "created_at" && f.value !== "") ? "filters-filled" : ""}`}>X</span>
                </div>
                <div className={"filter-item"}>
                    <label htmlFor="">Paire</label>
                    <select value={filterValue.some(f => f.field === "product_id") ? filterValue.find(f => f.field === "product_id").value : ""} className={"form-control history-filter"} onChange={(e) => onFilter("product_id", e.target.value)} name="" id="">
                        <option value={""}>Tous</option>
                        {optionsCurrency}
                    </select>
                    <span title={"Réinitialiser"} onClick={(e) => {e.stopPropagation(); onReset('product_id');}} className={`filters-reset ${filterValue.some(f => f.field === "product_id" && f.value !== "") ? "filters-filled" : ""}`}>X</span>
                </div>
            </div>}
            {!historyLoading ?
            <OrderTable totalSection={totalSection} responsive={true} className={`${filterVisible ? "filter-visible" : ""}`} actions={{delete: deleteOrder, update: onUpdate}} dataRef={historyRef} pair={pair} toShow={heads} orders={filtered} printable={printRef} printTitle={printTitle}>


                <button onClick={() => setFilterVisible(!filterVisible)} title={"Filtres"}  className={`btn ${filterValue.some((f) => f.value !== "") ?  "btn-info" : "btn-primary"} action-btn filter-plan`}>
                    <FilterIcon/>
                    <span title={"Tout Réinitialiser"} onClick={(e) => {e.stopPropagation(); setFilterValue([]); onFilterChange([])}} className={`filters-reset ${filterValue.some(f => f.value !== "") ? "filters-filled" : ""}`}>X</span>
                </button>

                <button onClick={() => onUpdate('', 'add')} title={"Ajouter un ordre à l'historique"} data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#insert-history" className={`btn btn-primary action-btn add-history`}>
                    <PlusCircleIcon/>
                </button>
                <AddHistoryModal type={modalType} toUpdate={toUpdate} onSave={saveOrder} />

                <button data-toggle="modal" data-target="#compute-tax" title={"Calculer plus value imposable"}  className={`btn btn-primary action-btn add-history`}>
                    <CalculatorIcon/>
                </button>
                <ComputeTaxModal onSubmit={handleTaxCalculation}/>

                <button onClick={handleRefresh} title={"Raffraichir"} className={`btn btn-primary action-btn add-history`}>
                    <RefreshIcon/>
                </button>

            </OrderTable> : <Loading message={<><div>Calculs en cours. Veuillez patienter...</div><div>Cette opération peut prendre quelques minutes. </div><ProgressBar percentage={progressCalcul}/></>} withoutOverlay={true}/>}
        </>

    )
}

export default History;