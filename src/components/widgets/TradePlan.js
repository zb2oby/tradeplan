import React,{useState, useEffect} from 'react';
import {
    archiveTPCollection,
    createTPCollectionIfNotExists,
    createTradePlan,
    deleteTradePlan,
    getAccount, getArchives,
    readAllTradePlan,
    updateTradePlan
} from "../../service/apiCalls";
import TradeButton from "../TradeButton";
import TradePlanModal from "../TradePlanModal";
import {parsePositionsAndTriggerSell} from "../../utils";
import TradePlanRow from "../TradePlanRow";
import Loading from "../Loading";
import ConfirmDeleteModal from "../confirmDeleteModal";
import {ArchiveIcon, PlusCircleIcon} from "../../icons/Icons";

const TradePlan  = ({pair, positions}) => {

    const [tradePlan, setTradePlan] = useState([]);
    const [portfolioCurr1, setPortfolioCurr1] = useState(0.00);
    const [portfolioCurr2, setPortfolioCurr2] = useState(0.00);
    const [previousCurr2, setPreviousCurr2] = useState(0.00);
    const [totalGainCurr1, setTotalGainCurr1] = useState(0.00);
    const [totalGainCurr2, setTotalGainCurr2] = useState(0.00);
    const [previousInvest, setPreviousInvest] = useState([]);
    const [tpModalType, setTpModalType] = useState('');
    const [tpModalDataKey, setTpModalDataKey] = useState('');
    const [lastDispo, setLastDispo] = useState({curr1: 0.00, curr2: 0.00});
    const [isLoading, setIsLoading] = useState(false);
    const [selectedArchive, setSelectedArchive] = useState(null);
    const [archiveList, setArchiveList] = useState([]);
    const [isArchiving, setIsArchiving] = useState(false);
    const [toUpdate, setToUpdate] = useState('');

    const [tempPortfolioCurr1, setTempPortfolioCurr1] = useState(0.00);
    const [tempPortfolioCurr2, setTempPortfolioCurr2] = useState(0.00);
    const [tempPreviousCurr2, setTempPreviousCurr2] = useState(0.00);

    let curr1 = pair.split('-')[1]; //eur
    let curr2 = pair.split('-')[0]; //eth

    useEffect(()=> {

        resetTradePlanView();

        getArchives(pair).then((r) => {

            if (typeof r !== "string") {
                let archives = r.map((c) => c.name);
                setArchiveList(archives);
            }

        });

    }, [pair, isArchiving]);

    useEffect(() => {

        let indexToFetch = `all_tradeplans-${pair}`;
        if (selectedArchive !== null) {
            indexToFetch = selectedArchive
        }

        /*if (archiveList.length > 0) {*/
            readAllTradePlan(indexToFetch, "TradePlan").then((res) => {
                if (res && res.length !== 0) {
                    let readableRes = res.map((res) => {
                        return res.data;
                    });
                    initFromStorage(readableRes);
                    let sorted = res.sort((a, b) => a.data.key < b.data.key ? -1 : 1);
                    setTradePlan(sorted);
                } else {
                    resetTradePlanView();
                    //console.log("previous", previousInvest)
                }
            });
       /* }*/

    }, [archiveList, selectedArchive]);

    useEffect(() => {
        if (positions.length > 0 && tradePlan.length === 0) {//TODO voir pourquoi le useEffect n'est pas déclencher à l'archivage
            //console.log("init", positions)
            initFromAccount();
            resetTradePlanView();
            let positions = positionAsTradePlan();
            setPreviousInvest(positions)
        }

    }, [positions])

    const positionAsTradePlan = () => {
        return positions.map((pos) => {
            return {
                trade_id: pos.trade_id,
                size: pos.size,
                price: pos.price,
                fee: pos.fee
            }
        });
    };

    const resetTradePlanView = () => {
        setTradePlan([]);
        setSelectedArchive(null);
        setTotalGainCurr2(0.00);
        setTotalGainCurr1(0.00);
    };

    const initFromStorage = (storedList) => {
        let lastPlan = storedList[storedList.length -1];
        setPortfolioCurr2(lastPlan.portfolioCurr2);
        setPreviousCurr2(lastPlan.previousCurr2);
        setPortfolioCurr1(lastPlan.portfolioCurr1);
        setPreviousInvest(lastPlan.previousInvest);
        setTotalGainCurr1(lastPlan.totalGainCurr1);
        setTotalGainCurr2(lastPlan.totalGainCurr2);
        setLastDispo({curr1: lastPlan.portfolioCurr1, curr2: lastPlan.portfolioCurr2})
    };

    const initFromAccount = (type) => {
        //recuperation du contenu actuel des portefeuilles de la paire
        getAccount("TradePlan").then(ac => {
            let currency1 = ac.find(a => a.currency === curr1);
            let currency2 = ac.find(a => a.currency === curr2);

            if (type === "update") {
                setTempPortfolioCurr1(parseFloat(currency1.balance));
                setTempPortfolioCurr2(parseFloat(currency2.balance));
                setTempPreviousCurr2(parseFloat(currency2.balance));
            } else {
                setPortfolioCurr1(parseFloat(currency1.balance));
                setPortfolioCurr2(parseFloat(currency2.balance));
                setPreviousCurr2(parseFloat(currency2.balance));
            }
            setLastDispo({curr1: parseFloat(currency1.balance), curr2: parseFloat(currency2.balance)})


        });

    };

    const fillOrder = (tradeId) => {

        let data = null;
        let today = new Date();

        let updated = tradePlan.map((t) => {
            if (t.data.key === tradeId ) {
                if (t.data.orderStatus !== undefined && t.data.orderStatus !== "filled") {
                    t.data.orderStatus = "filled";
                    t.data.filledAt = today.toLocaleDateString("fr-FR")
                } else {
                    t.data.orderStatus = "";
                    t.data.filledAt = "";
                }
                data = t;
            }
            return t;
        });
        updateTradePlan(data.ref["@ref"].id, data.data, pair);
        setTradePlan(updated);
    };

    const computeOrder = (data, temp) => {

        let temporary = {...temp};


        let plan = temporary.tempTradePlans;
        let prevInvest = temporary.tempPrevInvest;
        let gain, totalGCurr1, totalGCurr2, recu, tokens, prevCurr2, portCurr2, portCurr1 = 0.00;

        if (data.type === "sell" && prevInvest.length !== 0) {
            tokens = temporary.tempPortfolioCurr2 * data.quantite / 100; //la quantite a vendre
            /*console.log("prev", prevInvest)*/
            let trigger = parsePositionsAndTriggerSell(tokens, prevInvest);
            recu = (tokens * data.prix) - (tokens * data.prix * 0.005);
            /*console.log("trigger", trigger);*/
            gain = recu - trigger.computedBuyPrice;//la somme des plus petit prix trouvé a vendre dans l'historique
            prevInvest = trigger.newHistory;
            portCurr1 = temporary.tempPortfolioCurr1+ recu;
            portCurr2 = temporary.tempPortfolioCurr2 - tokens;
            prevCurr2 = temporary.tempPortfolioCurr2;
            totalGCurr1 = temporary.tempTotalGainCurr1 + gain;
            totalGCurr2 = temporary.tempsTotalGainCurr2;
        } else if (data.type === "buy") {
            console.log("data", data)
            tokens = temporary.tempPortfolioCurr1 * data.quantite / 100;
            recu = ((tokens - (0.005*tokens)) / data.prix); //0.005 ==> excepted fee
            //gain = recu - vendu (ou )
            let computePrev = computePrevious(plan);
            gain = (recu + computePrev.buys) - computePrev.sells - computePrev.lastTotalGainSells;
            //gain = recu - (previousCurr2 - portfolioCurr2);
            portCurr1 = temporary.tempPortfolioCurr1 - tokens;
            portCurr2 = temporary.tempPortfolioCurr2 + recu;
            let createID = Math.max.apply(Math,prevInvest.map((o) => o.trade_id)) + 1;
            //console.log("CREATEID", prevInvest, createID)
            prevInvest.unshift({trade_id: createID , size: recu, price: parseFloat(data.prix), fee: 0.005 * tokens});
            prevCurr2 = temporary.tempPortfolioCurr2;
            totalGCurr1 = temporary.tempTotalGainCurr1;
            totalGCurr2 = temporary.tempsTotalGainCurr2 + gain;
        }


        let tradeData = {
            key: plan.length > 0 ? plan[plan.length - 1].data.key +1 : 1,
            type: data.type,
            quantite: data.quantite,
            prix: data.prix,
            tokens: tokens,
            recu: recu,
            gains: gain,
            totalGainCurr1: totalGCurr1,
            totalGainCurr2: totalGCurr2,
            portfolioCurr2: portCurr2,
            portfolioCurr1: portCurr1,
            previousCurr2: prevCurr2,
            previousInvest: prevInvest,
            orderStatus: data.orderStatus !== "" ? data.orderStatus : "",
            filledAt: data.filledAt !== "" ? data.filledAt : ""
        };

        temporary.tempTradePlans.push({data: tradeData});
        temporary.tempPortfolioCurr1 = tradeData.portfolioCurr1;
        temporary.tempPortfolioCurr2 = tradeData.portfolioCurr2;
        temporary.tempPrevInvest = tradeData.previousInvest;
        temporary.tempPrevCurr2 = tradeData.previousCurr2;
        temporary.tempTotalGainCurr1 = tradeData.totalGainCurr1;
        temporary.tempsTotalGainCurr2 = tradeData.totalGainCurr2;


        return {
            data: tradeData,
            temp: temporary
        }

    };

    const computePrevious = (tempTradePlans) => {
        let totalPreviousSells = 0.00;
        let totalPreviousBuys = 0.00;
        let lastTotalGainsSells = 0.00;
        if (tempTradePlans.length !== 0) {
            lastTotalGainsSells = tempTradePlans[tempTradePlans.length -1 ].data.totalGainCurr2;
            tempTradePlans.forEach((prev) => {
                if (prev.data.type === "sell") {
                    totalPreviousSells += prev.data.recu / parseFloat(prev.data.prix)
                }
                if (prev.data.type === "buy") {
                    totalPreviousBuys += prev.data.recu
                }
            });
        }

        return {sells: totalPreviousSells, buys: totalPreviousBuys, lastTotalGainSells: lastTotalGainsSells};
    };

    const preparePromiseList = (toUpdate, temp) => {

        let temporary = {...temp};

        let delPromises = [];
        let updatePromises = [];

        //stocker les promise de suppression des ordres du tableau temporaire
        toUpdate.forEach((t) => {
            delPromises.push(deleteTradePlan(t.ref["@ref"].id, pair))
        });

        //preparer les nouveaux ordres mis à jour

        toUpdate.forEach((t) => {
            let oData = t.data;

            //TODO Voir si on recalcul le pourcentage pour tout les ordres à modifier ou seulement les FILLED
            //SEULEMENT POUR LES ORDRE DEJA FILLED : recalculer le pourcentage de risque afin de conserver la meme taille finale que celle réellement passée
            let newQuantite = oData.quantite;
            if (oData.orderStatus === "filled") {
                if (oData.type === "sell") {
                    newQuantite = oData.tokens / temporary.tempPortfolioCurr2 * 100;
                } else if (oData.type === "buy") {
                    newQuantite = oData.tokens / temporary.tempPortfolioCurr1 * 100;
                }
            }


            let o = {
                type: oData.type,
                quantite: newQuantite,
                prix: oData.prix,
                orderStatus: oData.orderStatus !== "" ? oData.orderStatus : "",
                filledAt: oData.filledAt !== "" ? oData.filledAt : ""
            };

            let tradeData =  computeOrder(o, temporary);
            temporary = tradeData.temp;

            //stocker les promise de submit de chaque ordreà repasser
            updatePromises.push(createTradePlan(tradeData.data, pair));
        });

        return {delpromises: delPromises, updatePromises: updatePromises}

    };

    const getTempValues = (tradeId) => {


        if (tpModalType === "update" && tradeId === 1) {
            return {
                tempPortfolioCurr1: tempPortfolioCurr1,
                tempPortfolioCurr2: tempPortfolioCurr2,
                tempPrevInvest: positionAsTradePlan(),
                tempPrevCurr2: tempPreviousCurr2,
                tempTotalGainCurr1: 0.00,
                tempsTotalGainCurr2: 0.00,
                tempTradePlans: []
            };
        }
        else if (tradeId === undefined) {
            return {
                tempPortfolioCurr1: portfolioCurr1,
                tempPortfolioCurr2: portfolioCurr2,
                tempPrevInvest: previousInvest,
                tempPrevCurr2: previousCurr2,
                tempTotalGainCurr1: totalGainCurr1,
                tempsTotalGainCurr2: totalGainCurr2,
                tempTradePlans: tradePlan.map((tr) => tr)
            };
        } else {

            let toRetain = tradePlan.filter((t) => t.data.key < tradeId);

            if (toRetain.length > 0) {
                let lastRetainedData = toRetain.sort((a, b) => a.data.key < b.data.key ? -1 : 1)[toRetain.length - 1].data;

                return {
                    tempPortfolioCurr1: lastRetainedData.portfolioCurr1,
                    tempPortfolioCurr2: lastRetainedData.portfolioCurr2,
                    tempPrevInvest: lastRetainedData.previousInvest,
                    tempPrevCurr2: lastRetainedData.previousCurr2,
                    tempTotalGainCurr1: lastRetainedData.totalGainCurr1,
                    tempsTotalGainCurr2: lastRetainedData.totalGainCurr2,
                    tempTradePlans: toRetain.map((ret) => ret)
                };
            } else {// cas du premier de la liste
                return getTempValues();
            }

        }


    };

    const processPromises = (promises, tradeId) => {

        const toRetain = tradePlan.filter((t) => t.data.key < tradeId);

        setIsLoading(true);
        Promise.all(promises.delpromises).then(() => {
            Promise.all(promises.updatePromises).then((responses) => {
                let proceeded = toRetain.concat(responses);

                //si la liste n'est pas vide on reinit from storage
                if (proceeded.length !== 0) {
                    let readableRes = proceeded.map((res) => {
                        return res.data;
                    });

                    initFromStorage(readableRes)
                } else { //sinon on reinit from account
                    resetTradePlanView();
                    initFromAccount()
                }
                //puis on met à jour setTradePlan
                setTradePlan(proceeded);
                setTpModalType('');
                setTpModalDataKey('');
                setIsLoading(false);
            });
        });
    };

    const deleteOrder = (tradeId) => {

        //trade à supprimer
        let deleted = tradePlan.filter((t) => t.data.key === tradeId);
        //Tableau temporaire des tradePlan dont la clé est supérieur à celle de celui à supprimer
        let toUpdate = tradePlan.filter((t) => t.data.key > tradeId);

        //on prend les valeurs à l'instant du dernier ordre conservé
        let tempValues = getTempValues(tradeId);

        let promises = preparePromiseList(toUpdate, tempValues);
        //stocker la promise de suppression de l'ordre en question
        promises.delpromises.push(deleteTradePlan(deleted[0].ref["@ref"].id, pair));

        //On lance dans l'ordre toute les promise en attente (celle de suppressioni et celle de creation)
        processPromises(promises, tradeId);

    };

    const saveOrder = (data) => {
        //console.log("SAVE", data)
        if (data.dataKey !== undefined) { //dataKey est ici la clé de l'ordre qui vient juste avant celui que l'on veut placer
            //Tableau temporaire des tradePlan dont la clé est supérieur à celle de celui après lequel ajouter le nouveau trade
            let toUpdate = tradePlan.filter((t) => t.data.key > data.dataKey);

            //on prepare l'ordre à enregistrer en prenant les valeur de l'ordre juste au dessus
            let tempValues = getTempValues(data.dataKey + 1); // revient à ça : let toRetain = tradePlan.filter((t) => t.data.key <= data.dataKey);

            //on realise l'ordre
            let tradeData = computeOrder(data, tempValues);

            //on l'neregistre
            let updatePromises = [];
            updatePromises.push(createTradePlan(tradeData.data, pair));

            //on l'ajoute aux valeur temporaires pour le recalcul des ordres suivants
            let newTempValues = tradeData.temp;
            //on realise les promises des ordres suivants à modifier
            let promises = preparePromiseList(toUpdate, newTempValues);
           // promises.updatePromises.forEach((p) => updatePromises.push(p));
            promises.updatePromises = updatePromises.concat(promises.updatePromises)

            //On lance dans l'ordre toute les promise en attente (celle de suppressioni et celle de creation)
            processPromises(promises, data.dataKey + 1 )

        } else { // sinon cas d'un ajout classique en dernieres position de la liste

            let plan = tradePlan.map((tr) => tr);

            let tempValues = getTempValues();

            let tradeData = computeOrder(data, tempValues);

            setIsLoading(true);
            createTPCollectionIfNotExists(pair).then((res) => {
                createTradePlan(tradeData.data, pair).then((res)=> {
                    plan.push(res);
                    let readableRes = plan.map((res) => {
                        return res.data;
                    });
                    initFromStorage(readableRes)
                    setTradePlan(plan);
                    setIsLoading(false);
                });
            })
        }
    };

    const updateOrder = (tradeId, newData) => {
        //trade à supprimer
        let updated = tradePlan.filter((t) => t.data.key === tradeId);
        //Tableau temporaire des tradePlan dont la clé est supérieur à celle de celui à supprimer
        let toUpdate = tradePlan.filter((t) => t.data.key > tradeId);

        let tempValues = getTempValues(tradeId);

        let updatedData = computeOrder(newData, tempValues);
        let newTempValues = updatedData.temp;

        let promises = preparePromiseList(toUpdate, newTempValues);
        promises.updatePromises.unshift(updateTradePlan(updated[0].ref["@ref"].id, updatedData.data, pair));

        processPromises(promises, tradeId);

    };

    const archiveTP = () => {
        archiveTPCollection(pair).then((res) => {
            //console.log(res)
            //resetTradePlanView();
            //TODO factoriser avec le useEffect identique mais non délenché
            initFromAccount();
            resetTradePlanView();
            let positions = positionAsTradePlan();
            setPreviousInvest(positions)
            setIsArchiving(!isArchiving);
        })
    };

    const initTpModal = (type, dataKey) => {

        setTpModalType(type);
        setTpModalDataKey(dataKey);

        if (type === "above") {
            let lastData = tradePlan.filter((t) => t.data.key === dataKey)[0].data;
            setLastDispo({curr1: lastData.portfolioCurr1, curr2:lastData.portfolioCurr2});
        }
        else if (type === "update") {
            let lastData = tradePlan.filter((t) => t.data.key === dataKey)[0].data;
            setToUpdate(lastData)
            if (dataKey === 1) {
                initFromAccount(type);
            } else {
                lastData = tradePlan.filter((t) => t.data.key === dataKey-1)[0].data ;
                setLastDispo({curr1: lastData.portfolioCurr1, curr2:lastData.portfolioCurr2});
            }
        }
        else {
            setLastDispo({curr1: portfolioCurr1, curr2: portfolioCurr2})
            setToUpdate('')
        }

    };

    const submitTrade = (type, data) => {
        if (type === "update") {
            updateOrder(data.dataKey, data);
        } else {
            saveOrder(data);
        }


    };

    const onSelectArchive = (arch) => {

        if (arch === null) {
            //TODO factoriser avec le useEffect identique mais non délenché
            initFromAccount();
            resetTradePlanView();
            let positions = positionAsTradePlan();
            setPreviousInvest(positions)
        }
        setSelectedArchive(arch);

    }

    const tpToSort = tradePlan.map(tp => tp); //clone du tradePlan pour l'affichage seulement
    const planList = tpToSort.sort((a,b) => a.data.key < b.data.key ? 1 : -1).map((tp, index) => {
        const data = tp.data;
        const lastKey = tpToSort[0].data.key;
        return (
            <TradePlanRow actionable={selectedArchive === null} key={index} data={data} updateOrder={initTpModal} addAbove={initTpModal} deleteOrder={deleteOrder} fillOrder={fillOrder} lastKey={lastKey} pair={pair}/>
        );
    });
    const archiveOptions = archiveList.map((a) => {
        const dateArch = new Date(parseInt(a.split("_")[2])).toLocaleDateString("Fr-fr");
        return (
            <button key={a} onClick={() => onSelectArchive(`${a}`)} className="dropdown-item" type="button">Archive {dateArch}</button>
        )
    });
    const dateArchiveSelected = selectedArchive ? new Date(parseInt(selectedArchive.split("_")[2])) : "";
    const titleArchiveSelected = dateArchiveSelected !== "" ? "- Archivé le " + dateArchiveSelected.toLocaleDateString("Fr-fr") + " " + dateArchiveSelected.toLocaleTimeString("FR-fr") : "";

    let rounded = 100;

    /*if (currentPrice < 1) {
        rounded = 1000000;
    }*/

    return (
        <>
            <div className="tp-header-group d-flex flex-row flex-wrap justify-content-around">
                <div className={"tp-header"}>
                    <div style={{textAlign: "center", borderBottom: "1px solid rgba(255,255,255,0.5)"}}>Disponibilités :</div>
                    <div>{Math.round(portfolioCurr1*rounded)/rounded} {curr1}</div>
                    <div>{Math.round(portfolioCurr2*rounded)/rounded} {curr2}</div>
                </div>
                <div className={"tp-header"}>
                    <div style={{textAlign: "center", borderBottom: "1px solid rgba(255,255,255,0.5)"}}>Gains:</div>
                    <div>{Math.round(totalGainCurr1*rounded)/rounded} {curr1}</div>
                    <div>{Math.round(totalGainCurr2*rounded)/rounded} {curr2}</div>
                </div>
            </div>

            <TradePlanModal toUpdate={toUpdate} tpType={tpModalType} dataKey={tpModalDataKey} pair={{curr1: curr1, curr2: curr2}} portfolio={lastDispo} onSubmit={submitTrade}/>

            <div className={"card-footer tp-footer"}>

                <span>Positions prévues <span className={"small"}>{titleArchiveSelected}</span></span>

                <div className={"action-tradeplan"}>

                    {selectedArchive === null &&
                    <button onClick={() => initTpModal('')} title={"Ajouter une prévision"}  data-toggle="modal" data-target="#addTradePlan" className={"btn btn-primary action-btn add-plan"}>
                        <PlusCircleIcon/>
                    </button>}


                    {tradePlan.length > 0 && selectedArchive === null &&
                    <>
                        <button title={"Archiver ce plan"}  data-toggle="modal" data-target={`#confirm-delete-archive`}  className={"btn btn-primary action-btn archive-plan"}>
                            <ArchiveIcon/>
                        </button>
                        <ConfirmDeleteModal dataKey={"archive"} onDelete={archiveTP} deleteButtonTxt={"Archiver"} cancelButtonTxt={"Annuler"}>
                            Souhaitez-vous archiver ce plan ?
                            <span style={{display: "block"}} className={"small"}>(L'archive sera consultable à tout moment)</span>
                        </ConfirmDeleteModal>
                    </>
                    }



                    {archiveList.length > 0 &&
                    <>
                        <button title={"Selection plan"} type="button" className="btn btn-sm btn-primary dropdown-toggle archive-plan" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" />
                        <div className="dropdown-menu">
                            <button onClick={() => onSelectArchive(null)} className="dropdown-item" type="button">Plan courant</button>
                            {archiveOptions}
                        </div>
                    </>}

                </div>
            </div>
            <div className={"table-responsive-perso history-table"}>
                <table className={""}>
                    <thead>
                    <tr>
                        <th>Type</th>
                        <th>Quantité</th>
                        <th>Taille</th>
                        <th>Prix</th>
                        <th>Reçu</th>
                        <th>Gains</th>
                        {/*<th>Passé</th>*/}
                        {selectedArchive === null ? <th>Actions</th> : <th>Etat</th>}
                    </tr>
                    </thead>
                    <tbody>
                    {planList}
                    </tbody>
                </table>

                {isLoading &&
                    <Loading withoutOverlay={true}/>
                }

            </div>

        </>
    )
};

export default TradePlan;
