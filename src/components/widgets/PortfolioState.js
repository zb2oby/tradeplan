import React, {useState, useEffect} from 'react';
import OrderTable from "../OrderTable";
import {getSymbol} from "../../utils";
import {ClipBoardIcon, CreditCardIcon} from "../../icons/Icons";
import ModalAddress from "../ModalAddress";


const PortfolioState = ({priceTime, currentPrice, onTest, positions, feedState, pair}) => {

    const [portfolioCurr2, setPortfolioCurr2] = useState(0.00);
    const [moyPonderee, setMoyPonderee] = useState(0.00);
    const [totalInvest, setTotalInvest] = useState(0.00);
    const [testPrice, setTestPrice] = useState(0.00);
    const [potentialGain, setPotentialGain] = useState(0.00);
    const [potentialAllIn, setPotentialAllIn] = useState(0.00);
    const [adressVisible, setAdressVisible] = useState(false);


    let curr1 = pair.split('-')[1]; //eur
    let curr2 = pair.split('-')[0]; //eth

    useEffect(() => {
        setPortfolioCurr2(0.00);
        setMoyPonderee(0.00);
        setTotalInvest(0.00);
        setPotentialGain(0.00);
        setPotentialAllIn(0.00);
    }, [pair]);

    useEffect(() => {

        let totalTokenB = 0.00;
        let moyPond = 0.00;
        let totalInv = 0.00;
        positions.forEach((order) => {
            totalInv += Math.abs( (parseFloat(order.size) * parseFloat(order.price)) + parseFloat(order.fee) );
            totalTokenB+= parseFloat(order.size);

        });
        positions.forEach((order) => {
            moyPond += (parseFloat(order.size) / totalTokenB * order.price)
        });
        setTotalInvest(totalInv);
        setMoyPonderee(moyPond);
        setPortfolioCurr2(totalTokenB);


    }, [positions]);


    useEffect(() => {

        calcPotentiel(portfolioCurr2, currentPrice, testPrice, totalInvest)

    }, [currentPrice, testPrice, totalInvest, portfolioCurr2]);


    const calcPotentiel = (portCurr2, currentP, testP, totalI) => {
        let potentialG = (portCurr2 * (testP > 0 ? testP : currentP)) - totalI;
        let potentialAllI = totalI + potentialG;

        setPotentialAllIn(potentialAllI);
        setPotentialGain(potentialG);
    };

    const heads = [
        {id: "side", label: "Type"},
        {id: "created_at", label: "Date"},
        {id: "size", label: `Taille`},
        {id: "price", label: `Prix`}
    ];

    let dateTimePrice = new Date(priceTime);
    let today = new Date();
    let datePrice = "Aujourd'hui";
    if (dateTimePrice.toLocaleDateString("Fr-fr") !== today.toLocaleDateString("Fr-fr")) {
        datePrice = dateTimePrice.toLocaleDateString("Fr-fr");
    }
    let timePrice = dateTimePrice.toLocaleTimeString("Fr-fr");

    let rounded = 100;
    if (currentPrice < 1) {
        rounded = 1000000;
    }

    let portfolio2Round = 100;
    if (portfolioCurr2 < 1) {
        portfolio2Round = 1000000;
    }

    return (
        <div className={"state-content"}>

            <div className={"button-address"}>
                <button title={`Obtenir adresse ${pair.split('-')[0]}`} data-toggle="modal" data-target="#display-address" data-backdrop="static" data-keyboard="false" className={'btn btn-primary action-btn'} onClick={() => setAdressVisible(true)}><CreditCardIcon/></button>
                <ModalAddress visible={adressVisible} pair={pair} callBack={() => setAdressVisible(false)}/>
            </div>
            <div className={"state-group d-flex flex-column flex-sm-row flex-wrap justify-content-between"}>

                <div className={"state-item flex-fill d-flex flex-column flex-wrap justify-content-between"}>
                    <div className={"state-line flex-fill"}>
                        <span className={"state-label"}>Prix actuel ({curr2})</span>
                        <span className={"state-label"} style={{fontSize: '0.6em'}}>{datePrice} - {timePrice}</span>
                        <span className={"big-price state-data"}>{Math.round(currentPrice * rounded) / rounded} {getSymbol(curr1)}</span>
                    </div>
                    <div className={"flex-fill d-flex flex-row flex-wrap justify-content-between align-items-bottom"}>
                        <div className={"form-group state-line flex-fill"}>
                            <span className={"state-label"}>Tester les gains pour un prix :</span>
                            <input type={"number"} placeholder={`Renseignez un prix`} onChange={(e) => setTestPrice(e.target.value)} className={"form-control state-data"}/>
                        </div>
                    </div>
                </div>

                <div className={"state-item flex-fill d-flex flex-column flex-wrap justify-content-between"}>
                    <div className={"state-line d-flex flex-row flex-wrap justify-content-around"}>
                        <div><span className={"state-label"}>Taille entrées {curr2}</span> {Math.round(portfolioCurr2 * portfolio2Round) / portfolio2Round}</div>
                        <div><span className={"state-label"}>Valeur Achat</span> {Math.round(totalInvest * rounded) / rounded} {getSymbol(curr1)}</div>
                        <div><span className={"state-label"}>prix moyen</span> {Math.round(moyPonderee * rounded) / rounded} {getSymbol(curr1)}</div>
                    </div>
                    <div className={"state-line d-flex flex-row flex-wrap justify-content-around"}>
                        <div><span className={"state-label"}>Valeur Actuelle</span> <span style={{color: `${potentialGain < 0 ? "red" : "green"}`}}>{Math.round(potentialAllIn * rounded) / rounded} {getSymbol(curr1)}</span></div>
                        <div><span className={"state-label"}>Gain (All-in)</span> <span  style={{color: `${potentialGain < 0 ? "red" : "green"}`}}>{Math.round(potentialGain * rounded) / rounded   } {getSymbol(curr1)}</span></div>
                    </div>
                </div>
            </div>


            <div>
                <div className="card-footer mt-3">Entrées non liquidées</div>
                <OrderTable orders={positions} toShow={heads} />
            </div>



        </div>
    )
}

export default PortfolioState;