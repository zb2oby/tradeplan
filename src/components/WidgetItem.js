import React from 'react';
import {ChevronExpandIcon} from "../icons/Icons";


const WidgetItem = ({titleComp, title, children, collapse, collpaseId, className}) => {

    return (
            <div className={`card-perso flex-fill ${className ? className : ''}`}>
                {collapse ?
                    <a data-toggle="collapse" className={"collapse-link"} href={`#${collpaseId}`} role="button">
                        <div className="card-header">
                            {title} - {titleComp !== undefined && <span className={"widget-title-comp"}>{titleComp}</span>}
                            <span className={"collapse-icon"}>
                                <ChevronExpandIcon/>
                            </span>
                        </div>
                    </a>
                :
                    <div className="card-header">
                        {title}{titleComp !== undefined && <span className={"widget-title-comp"}>{titleComp}</span>}
                    </div>
                }


                <div id={collapse ? collpaseId : ''} className={`card-body ${collapse ? "collapse" : ''}`}>
                    {children}
                </div>
            </div>
    )
}

export default WidgetItem;