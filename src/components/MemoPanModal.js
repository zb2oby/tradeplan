import React from 'react';

const MemoModal = () => {

    let dataWaves = [
        {
            title: "Vague 3",
            label: "% de prolongation de la vague 1 au creux de la vague 2",
            data: [
                {fibo: 2.618, proba: 2},
                {fibo: 2, proba: 3},
                {fibo: 1.618, proba: 4},
                {fibo: 1.382, proba: 3},
                {fibo: 1.236, proba: 1},
                {fibo: 1, proba: 3},
                {fibo: 0.618, proba: 2}
            ]
        },
        {
            title: "Vague 5",
            label: "% de prolongation de la vague 1 par le sommet de la vague 3 (*[num à num])\n" + "OU % de prolongation de la vague 3 par le creux de la vague 4 (*num)",
            data: [
                {fibo: 0.618, proba: 4, info: "*[1 à 3]"},
                {fibo: 1, proba: 1, info: "*[1 à 3]"},
                {fibo: 1, proba: 3, info: "*1"},
                {fibo: 0.618, proba: 3, info: "*3"}
            ]
        },
        {
            title: "Vague C",
            label: "% de prolongation de la vague A par le creux de la B",
            data: [
                {fibo: 2, proba: 1},
                {fibo: 1.618, proba: 3},
                {fibo: 1.382, proba: 4},
                {fibo: 1.236, proba: 3},
                {fibo: 1, proba: 4},
                {fibo: 0.618, proba: 3}
            ]
        },
        {
            title: "Vague B",
            label: "% de retracement de la vague A",
            data: [
                {fibo: 1.236, proba: 1},
                {fibo: 1, proba: 1},
                {fibo: 0.764, proba: 2},
                {fibo: 0.618, proba: 4},
                {fibo: 0.5, proba: 4},
                {fibo: 0.382, proba: 2},
                {fibo: 0.236, proba: 2},
            ]
        },
        {
            title: "Vague 2",
            label: "% de retracement de la vague 1",
            data: [
                {fibo: 0.236, proba: 2},
                {fibo: 0.382, proba: 3},
                {fibo: 0.5, proba: 3},
                {fibo: 0.618, proba: 4},
                {fibo: 0.764, proba: 2},
                {fibo: 1, proba: 1},

            ]
        },
        {
            title: "Vague 4",
            label: "% de retracement de la vague 3",
            data: [
                {fibo: 0.236, proba: 3},
                {fibo: 0.382, proba: 4},
                {fibo: 0.5, proba: 3},
                {fibo: 0.618, proba: 1},
                {fibo: 0.764, proba: 1},
                {fibo: 1, proba: 1},

            ]
        },
    ];

    const eliottData = dataWaves.map((w, wi) => {



        const fiboRow = w.data.map((r, ri) => {
            let stringifyProba = "";
            for(let i = 0; i < r.proba; i++) {
                stringifyProba += "*";
            }
            return (
                <li className={"fibo-li"} key={`${wi}-${ri}`}>
                    <span className={`fibo-data fibo-number ${r.proba === 4 ? "max" : ""}`}>{r.fibo} {r.info && r.info}</span>
                    <span className={`fibo-data fibo-proba ${r.proba === 4 ? "max" : ""}`}>{stringifyProba}</span>
                </li>
            )
        });

       return  (
           <div className={"fibo-content"} key={wi}>
               <div className={"fibo-header"}>
                   <div className={"fibo-title"}>{w.title}</div>
                   <label className={"small"}>{w.label}</label>
               </div>
               <div>
                   <ul className={"fibo-list"}>
                       {fiboRow}
                   </ul>
               </div>
           </div>

       )
    });

    return (
        <div className="modal fade" id={`memoModal`} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <button type="button" className="close close-memo" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <div className="modal-body">
                        <ul className="nav nav-tabs" id="myTab" role="tablist">
                            <li className="nav-item">
                                <a className="nav-link active" id="home-tab" data-toggle="tab" href="#eliottmemo" role="tab">Eliott / Fibo</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" id="profile-tab" data-toggle="tab" href="#ordermemo" role="tab">Type ordres</a>
                            </li>
                        </ul>
                        <div className="tab-content" id="memoContent">
                            <div className="tab-pane fade show active" id="eliottmemo" role="tabpanel">
                                <div className={"disclaim"}>
                                    <p>Ci-dessous les valeurs de retracement et prolongation de fibonnacci triées par probabilités subjectives</p>
                                    <p>Ces données proviennent de l'observation par l'experience et ne sont qu'indicatives</p>
                                </div>

                                {eliottData}
                            </div>
                            <div className="tab-pane fade" id="ordermemo" role="tabpanel">
                                <div className={"disclaim"}>
                                    <p>Ci-dessous l'explication des ordres existants sur Coinbase Pro</p>
                                </div>
                                <ul>
                                    <li>
                                        Market
                                        <p className={"small"}>Ordre au prix actuel du marché.</p>
                                    </li>
                                    <li>
                                        Limit
                                        <p className={"small"}>Ordre qui se déclenchera à un prix donné, si le prix continue de changer, l'ordre peut tout à fait être passé à un prix différent de la limite fixée.</p>
                                    </li>
                                    <li>
                                        Stop-limit
                                        <p className={"small"}>Donne l'ordre de passer un ordre Limit à partir d'un pallier (stop) donné.</p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="modal-footer">
                        <button type="button"  className="btn btn-primary" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default MemoModal;