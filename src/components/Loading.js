import React from 'react';


const Loading = ({style, above, withoutOverlay, message}) => {
    return (
        <div className={`text-center text-primary`}>
            {!withoutOverlay && <div className={"overlay visible"} />}
            <div style={style} className={`spinner-border spinner-border m-2 ${above ? "above" : ""}`} role="status">
                <span className="sr-only">Loading...</span>
            </div>
            <div>{message}</div>
        </div>
    )
};

export default Loading;