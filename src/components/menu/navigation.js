import React, {useState} from 'react';
import "./menu.css";
import {HamburgerIcon} from "../../icons/Icons";


const Navigation = ({children, className}) => {

    const [menuClass, setMenuClass] = useState(false)

    return (

        <div className={`main-menu ${className ? className : ""}`}>
            <div className={`overlay ${menuClass ? "visible" : ""}`} onClick={() => setMenuClass(!menuClass)}/>
            <div className={`hamburger`} onClick={() => setMenuClass(!menuClass)}>
                <HamburgerIcon/>
            </div>
            <nav className={`navigation ${menuClass ? "visible" : ""}`}>
                {children}
            </nav>
        </div>)
}

export default Navigation