import React from 'react';
import ConfirmDeleteModal from "./confirmDeleteModal";
import {ArrowBarUpIcon, CheckCircleIcon, DeleteIcon, PencilSquareIcon} from "../icons/Icons";

const TradeButton = ({onUpdate, actionable, status, onFill, onDelete, lastOrder, dataKey, onAddAbove, statusDate}) => {

    return (
        <>
            <div className={"action-btns"}>
                <button title={"Mettre à l'état passé"} onClick={onFill} disabled={!actionable} className={`action-btn btn btn-${status === "filled" ? "success" : "secondary"}`}>
                    <CheckCircleIcon/>
                </button>
                {actionable &&
                <button title={"Modifier"} onClick={onUpdate} data-toggle="modal" data-target="#addTradePlan" className={`action-btn btn btn-warning`} style={{color: "#fff"}}>
                    <PencilSquareIcon/>
                </button>}
                {actionable &&
                <button title={"Supprimer"} data-toggle="modal" data-target={`#confirm-delete-${dataKey}`} className={`action-btn btn btn-danger`} disabled={status === "filled"  }>
                    <DeleteIcon/>
                </button>}
                {!lastOrder && actionable &&
                <button onClick={onAddAbove} title={"Insérer au dessus"} data-toggle="modal" data-target="#addTradePlan" className={`action-btn btn btn-info`}>
                    <ArrowBarUpIcon/>
                </button>}
                <ConfirmDeleteModal dataKey={dataKey} onDelete={onDelete} lastOrder={lastOrder} deleteButtonTxt={"Supprimer"} cancelButtonTxt={"Annuler"}/>
            </div>
            <div><p className={"smallDate"}>{statusDate ? statusDate : ""}</p></div>
        </>
    )
}

export default TradeButton;