import React from 'react';
import Navigation from "./menu/navigation";
import {supportedPairs} from "./Constants";
import AllPortfolios from "./AllPortfolios";


const AppHeader = ({defaultCurr, pair, callBackSelect, callBackSign, callBackProfile, otherPrices}) => {

    const options = supportedPairs.map((p,i) => <option key={i} value={p}>{p}</option>);

    return (
        <header className={"app-header"}>
            <Navigation className={"app-header-item"}>
                <div className={"a-nav"} onClick={() => callBackProfile()} data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#modalSign" >Mes infos</div>
                <div className={"a-nav"} data-toggle="modal" data-target="#memoModal">Mémos</div>
                <div className={"a-nav"} onClick={callBackSign} >Déconnexion</div>
            </Navigation>
            <div className={"header-select app-header-item"}>

                <div className="d-flex flex-row flex-wrap justify-content-around align-items-center">
                    <div>
                        <label htmlFor="">Paire</label>
                    </div>
                    <div>
                        <select value={pair} onChange={(e) => callBackSelect(e.target.value)} className={"form-control"}>
                            <option value="" disabled={true}>Sélectionnez une paire</option>
                            {options}
                        </select>
                    </div>

                </div>
            </div>
            <AllPortfolios className={"app-header-item"} defaultCurr={defaultCurr} pair={pair} otherPrices={otherPrices}/>
        </header>
    )
};

export default AppHeader;