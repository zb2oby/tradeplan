/* global gapi */
import React, {useState, useEffect} from 'react';
import './App.css';
import Portfolio from "./Portfolio";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/js/bootstrap.js';
import Signin from "./Signin";
import {checkAuth, signup, updateUser} from "../service/apiCalls";
import Alert from "./Alert";
import SignUpModal from "./SignUpModal";
import MemoModal from "./MemoPanModal";
import AppHeader from "./AppHeader";

const App = () => {

    const [pair, setPair] = useState('');
    const [defaultCurr, setDefaultCurr] = useState('');
    const [favorite, setFavorite] = useState('');
    const [isSignedIn, setIsSignedIn] = useState(false);
    const [alert, setAlert] = useState(null);
    const [gUser, setGUser] = useState(null);
    const [webSocketClient, setWebSocketClient] = useState(null);
    const [typeModal, setTypeModal] = useState('');
    const [otherPrices, setOtherPrices] = useState([]);

    let signinID = process.env.REACT_APP_GOOGLE_SIGNIN_ID;
    //let signinID = process.env.REACT_APP_GOOGLE_SIGNIN_ID_DEV

    useEffect(() => {
        //setTimeout to prevent gapi undefined
        const ischecked = sessionStorage.getItem("stoploss") !== null && JSON.parse(sessionStorage.getItem("stoploss")).check;

            setTimeout(() => {
                let auth2;
                window.gapi.load('auth2', () => {
                    auth2 = gapi.auth2.init({
                        client_id: signinID
                    })

                    /*auth2.then(() => {
                        setIsSignedIn( auth2.isSignedIn.get())
                    });*/
                });

                window.gapi.load('signin2', function() {
                    if (!isSignedIn || !ischecked) {
                        getLoginButton();
                    }
                });
            }, 500);

    }, []);


    const getLoginButton = () => {

        let opts = {
            width: 200,
            height: 50,
            theme: 'dark',
            client_id: signinID,
            onsuccess: signIn
        };

        gapi.signin2.render('loginButton', opts)

    }

    const signIn = (user) => {
        setAlert(null);
        let forceCall = false;
        let token;
        //sign in appelé depuis le bouton
        if (user !== undefined) {
            token = user.getAuthResponse().id_token;
        }
        //sign in appelé depuis l'update profile
        else {
            let auth2 = gapi.auth2.getAuthInstance();
            token = auth2.currentUser.get().getAuthResponse().id_token;
            forceCall = true;
            if (webSocketClient !== null) {
                console.log('Closing socket');
                webSocketClient.close();
            }

        }

        checkAuth(token, forceCall).then((res)=> {
            if (res) {
                //dans le cas d'un refresh du token auth n'est pas appelé et on reçoit la session raffraichie
                if (res.refresh) {
                    setIsSignedIn(true);
                    setPair(JSON.parse(sessionStorage.getItem('stoploss')).favorite);
                    setDefaultCurr(JSON.parse(sessionStorage.getItem('stoploss')).defaultCurr);
                    return;
                }
                //dans le cas d'une auth initiale on set la session
                if (res.check === true) {
                    sessionStorage.setItem("stoploss", JSON.stringify(res));
                    setIsSignedIn(true);
                    setPair(res.favorite);
                    setDefaultCurr(res.defaultCurr);
                    setFavorite(res.favorite);
                }
                //si la reponse est false on affiche la modal d'inscrption
                else {
                    setGUser(res.gId);
                    setTypeModal("signup");
                    let btnmodal = document.getElementById("btsign");
                    btnmodal.click();
                }
            }
        }).catch((e) => {
            let alert = <Alert type={"warning"} reason={`Une erreur est survenue : ${e.message}`} message={"Veuillez contacter le support."} onDismiss={setAlert}/>
            setAlert(alert);
        })

    };

    const signUp = (data) => {
        data = {...data, gId: gUser};
        //call signup function back
        signup(data).then((res) => {
            if (res) {
                let alert = <Alert type={"success"} reason={"Votre compte a bien été crée ! "} message={"Suivez et planifiez vos trade en toute simplicité !"} onDismiss={setAlert}/>
                setAlert(alert);
                setTypeModal('');
                setTimeout(() => {
                    getLoginButton();
                }, 2000)
            }

        })
    }

    const signOut = () => {

        if (webSocketClient !== null) {
            console.log('Closing socket');
            webSocketClient.close();
        }

        console.log('Closing session');
        sessionStorage.clear();
        let auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(() => {
            setIsSignedIn(auth2.isSignedIn.get());
            getLoginButton();
        });
        auth2.disconnect();
    };

    const showProfile = () => {
        setTypeModal('profile');
        let nav = document.getElementsByClassName("navigation")[0];
        let overlay = document.getElementsByClassName("overlay")[0];
        nav.classList.remove('visible');
        overlay.classList.remove('visible');
    };


    const updateProfile = (data) => {
        updateUser(data).then((res) => {
            if (res) {
                let alert = <Alert className={"message"} type={"success"} reason={"Votre compte a bien été mis à jour ! "} onDismiss={setAlert}/>
                setAlert(alert);
                setTypeModal('');
                setTimeout(() => {
                    signIn();
                }, 2000)
            }

        })
    };

    return (
        <div className="app-container">
            <button style={{visibility: "hidden", position: "absolute", zIndex: "-1000"}} title={"Signup"}  id={"btsign"} data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#modalSign" className={"btn btn-primary action-btn add-plan"}>Créer un compte</button>
            <SignUpModal session={sessionStorage.getItem('stoploss')}  onSubmit={typeModal === "signup" ? signUp : updateProfile} onCancel={typeModal === "signup" && signOut} type={typeModal}/>
            <MemoModal/>
            {alert}
            {!isSignedIn ? <Signin/> : null}
            {isSignedIn ?
            <>
                <AppHeader defaultCurr={defaultCurr} pair={pair} callBackSelect={setPair} callBackSign={signOut} callBackProfile={showProfile} otherPrices={otherPrices} />
                <Portfolio otherPrices={otherPrices} setOtherPrices={setOtherPrices} pair={pair} deferSocket={setWebSocketClient}/>
            </> : null}

        </div>
    );
}

export default App;