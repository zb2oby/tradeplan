import React from 'react';
import Helmet from 'react-helmet';

const TitleComponent = ({ title, icon }) => {
    var defaultTitle = '⚛️ app';

    let iconCurr1 = icon.split("-")[0];

    return (
        <Helmet
            defer={false}
            link={[
                {"rel": "icon",
                    "type": "image/png",
                    "href": `${iconCurr1}.ico`
                }
            ]}
        >
            <title>{title ? title : defaultTitle}</title>
            {/*<link rel={"icon"} type={"image/png"} href={`${pair}.ico`} sizes={"16x16"}/>*/}
        </Helmet>
    );
};

export { TitleComponent };