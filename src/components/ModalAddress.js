import React, {useState, useEffect} from 'react';
import {
    createAddress,
    createAddressesCollectionIfNotExists,
    generateAddress,
    getCoinbaseAccounts,
    getExistingAddress
} from "../service/apiCalls";
import Loading from "./Loading";
import {ClipBoardIcon} from "../icons/Icons";
import Alert from "./Alert";

const ModalAddress = ({visible, pair, callBack}) => {

    const [address, setAddress] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [alert, setAlert] = useState(null);


    useEffect(() => {
        if (visible) {
            queryWalletAddress();
        }
    }, [visible]);

    const queryWalletAddress = () => {

        setIsLoading(true);
        getExistingAddress(pair).then((r) => {
            if (r !== '') {
                setAddress(r);
                setIsLoading(false);
            } else {
                createAddressesCollectionIfNotExists().then((res) => {
                    getCoinbaseAccounts().then((cb) => {
                        let acount = cb.filter((ac) => ac.currency === pair.split('-')[0]);
                        generateAddress(acount[0].id).then((adr) => {
                            let data = {currency: pair.split('-')[0], address: adr.address}
                            createAddress(data).then((ad) => {
                                setAddress(adr.address);
                                setIsLoading(false);
                            });
                        });

                    })
                });
            }
        })

    };

    const handleCopy = () =>  {
        navigator.clipboard.writeText(address).then(function() {
            let alert = <Alert className={"message"} type={"success"} reason={"Adresse copiée dans le presse-papier !"} onDismiss={setAlert}/>
            setAlert(alert);
            //console.log('Async: Copying to clipboard was successful!');
        }, function(err) {
            console.error('Async: Could not copy text: ', err);
        });
    };

    return (
        <div className="modal fade" id={`display-address`} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h6 className="modal-title" id="exampleModalLongTitle">Adresse de dépôt coinbase Pro pour {pair.split('-')[0]}</h6>
                    </div>
                    <div className="modal-body">
                        {alert}
                        {isLoading ?
                            <Loading withoutOverlay={true}/>
                            :
                            <div>{address} <span onClick={handleCopy} title={"Copier dans le presse papier"} className={"clipBoard"}><ClipBoardIcon/></span></div>
                        }
                    </div>
                    <div className="modal-footer">
                        <button type="button"  className="btn btn-primary" data-dismiss="modal" onClick={()=> {setAddress(null); callBack()}}>Fermer</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default ModalAddress;