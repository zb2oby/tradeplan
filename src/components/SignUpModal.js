import React, {useState, useEffect} from 'react';
import Alert from "./Alert";
import jwt from "jsonwebtoken";
import {currencies, supportedPairs} from "./Constants";
import {hideModal} from "../utils";
import {Form, FormItem, Select} from 'react-form-lib';
import {signupFormRules} from "../constants/validationRules";

const areEqual = (prevProps, nextProps) => {
    return prevProps.type === nextProps.type
    //return true
};

const SignUpModal = React.memo(({onSubmit, onCancel, type, session}) => {

    const [favorite, setFavorite] = useState('');
    const [defaultCurr, setDefaultCurr] = useState('');

    useEffect(() => {


        if (session !== null) {
            const favo = JSON.parse(session).favorite;
            const defoCurr = JSON.parse(session).defaultCurr;

            setFavorite(favo);
            setDefaultCurr(defoCurr);
        }

    }, [session]);


    const submit = (dat) => {

        let tokenCbSecret = '';
        if (dat.cbSecret !== '') {
            tokenCbSecret = jwt.sign({
                secret: dat.cbSecret
            }, process.env.REACT_APP_JWT_SECRET);
        }
        return {...dat, cbSecret: tokenCbSecret};
    };

    const handleSubmit = (data) => {
        onSubmit(submit(data));
        hideModal("modalSign");
    };


    let title = type === "signup" ? "Renseignez quelques informations pour vous inscrire" : "Renseignez vos nouvelles informations";
    let labelCancel = type === "signup" ? "Pas interessé" : "Annuler";
    let labelSubmit = type === "signup" ? "Créer mon compte" : "Mettre à jour";

    let placeHolder = type === "profile" ? "Si nécessaire, entrez vos nouvelles informations API" : ""

    const optionsCurrency = supportedPairs.map((p) => ({label: p, value: p}));
    const optionsDefaultCurr = currencies.map((p) => ({label: p.curr, value: p.curr}));

    return (
        <div className="modal fade" id="modalSign" tabIndex="-1" role="dialog">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    {type === "signup" && <Alert type={"warning"} reason={"Vôtre compte est inconnu."} message={"Veuillez vous identifier avec un autre compte ou inscrivez-vous avec ce compte."}/>}
                    <div className="modal-header">
                        <h5 className="modal-title" id="modalSignTitle">{title}</h5>
                    </div>
                    <div className="modal-body">
                        <Form validationRules={signupFormRules} >
                            <FormItem
                                name={"cbAccess"}
                                label={"Clé d'accès Coinbase Pro"}
                                show={() => true}
                                type={"text"}
                                className={"form-group"}
                            >
                                <input placeholder={placeHolder} className={"form-control"}/>
                            </FormItem>
                            <FormItem
                                name={"cbPass"}
                                label={"PassPhrase Coinbase Pro"}
                                show={() => true}
                                type={"text"}
                                className={"form-group"}
                            >
                                <input placeholder={placeHolder} className={"form-control"}/>
                            </FormItem>
                            <FormItem
                                name={"cbSecret"}
                                label={"Clé secrète Coinbase Pro"}
                                sublabel={"(votre clé sera cryptée automatiquement et ne pourras être décryptée que par l'api Coinbase)"}
                                show={() => true}
                                type={"text"}
                                className={"form-group"}
                            >
                                <input placeholder={placeHolder} className={"form-control"}/>
                            </FormItem>
                            <FormItem
                                name={"favorite"}
                                label={"Sélectionnez votre paire favorite"}
                                show={() => true}
                                initialValue={favorite}
                                required={true}
                                type={"select"}
                                className={"form-group"}
                            >
                                <Select placeHolder={"Sélectionnez une paire"} dataSet={optionsCurrency} className={"form-control"}/>
                            </FormItem>
                            <FormItem
                                name={"defaultCurr"}
                                label={"Sélectionnez votre monnaie d'échange par defaut"}
                                initialValue={defaultCurr}
                                show={() => true}
                                required={true}
                                type={"select"}
                                className={"form-group"}
                            >
                                <Select placeHolder={"Sélectionnez une monnaie"} dataSet={optionsDefaultCurr} className={"form-control"}/>
                            </FormItem>
                            <div className={"btn-toolbar"} role={"toolbar"}>
                                <FormItem
                                    show={() => true}
                                    callBackAction={handleSubmit}
                                    type={"submit"}
                                >
                                    <button type={"button"} className={"btn btn-primary mr-2"}>{labelSubmit}</button>
                                </FormItem>
                                <FormItem
                                    show={() => true}
                                    type={"reset"}
                                >
                                    {onCancel ? <button id="dismiss-modalSign" data-dismiss="modal" onClick={onCancel} className={"btn btn-primary mr-2"}>{labelCancel}</button>
                                        : <button id="dismiss-modalSign" data-dismiss="modal" className={"btn btn-primary mr-2"}>{labelCancel}</button>
                                    }
                                </FormItem>
                            </div>
                        </Form>
                        <div className={"p-2"}>
                            <a className={"small"} target={"_blank"} href="https://help.coinbase.com/en/pro/other-topics/api/how-do-i-create-an-api-key-for-coinbase-pro.html">Aide : Comment créer des clé api coinbase pro</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}, areEqual);


export default SignUpModal