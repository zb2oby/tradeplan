import React from 'react';
import ConfirmDeleteModal from "./confirmDeleteModal";
import {DeleteIcon, PencilSquareIcon, UpdateIcon} from "../icons/Icons";

const HistoryAction = ({onUpdate, onDelete, dataKey}) => {

    return (
        <>
            <div className={"action-btns"}>
                <button title={"Modifier"} onClick={onUpdate} className={`action-btn btn btn-warning`} data-toggle="modal" data-target="#insert-history" style={{color:"#fff"}}>
                    <PencilSquareIcon/>
                </button>
                <button title={"Supprimer"} data-toggle="modal" data-target={`#confirm-delete-${dataKey}`} className={`action-btn btn btn-danger`} >
                    <DeleteIcon/>
                </button>
                <ConfirmDeleteModal dataKey={dataKey} onDelete={onDelete} deleteButtonTxt={"Supprimer"} cancelButtonTxt={"Annuler"}/>
            </div>
        </>
    )
}

export default HistoryAction;