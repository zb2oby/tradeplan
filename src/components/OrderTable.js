import React, {useState, useEffect} from 'react';
import {displayRoundWith10ToMinus, getSymbol, isFiat} from "../utils";
import ReactToPrint from "react-to-print";
import HistoryAction from "./HistoryAction";
import {PrintIcon} from "../icons/Icons";

const OrderTable = ({totalSection, responsive, className, dataRef, toShow, orders, children, printable, printTitle, actions}) => {



    const [originalTitle, setOriginalTitle] = useState('');

    useEffect(() => {
        if (printable) {
            setOriginalTitle(printable.current.ownerDocument.title)
        }
    }, []);

    const theads = toShow.map((h) => {
        return (
            <th key={h.label}>
                {h.label}
            </th>
        )
    });

    const searchElt = (term) => {
        return toShow.filter((tr) => tr.id === term).length > 0;
    };


    const orderList = orders.map((order, index) =>  {

        let rounded = 1000;

        if (order.price < 1) {
            rounded = 1000000;
        }

        const pair = order.product_id;
        let curr1 = pair.split('-')[1]; //eur
        let curr2 = pair.split('-')[0]; //eth
        let date = new Date(order.created_at.split("T")[0]);

        let titleExternal = ""
        if (order.thing === "otherCurr" || (!isFiat(order.thing) && order.thing !== "other")) {
            titleExternal = "crypto"
        } else {
            titleExternal = "bien/service"
        }

        return  (
        <tr key={index} >
            {searchElt("side") && <td title={` ${order.converted ? "Partiellement convertit": ""} ${order.sellType ? "Cession Externe " + titleExternal: ""}`} className={`${order.side === "sell" ? "sell": "buy"} ${order.converted ? "converted": ""} ${order.sellType ? "external": ""}`}>{order.side === "sell" ? order.sellType && order.sellType === "external" ? "Cession" : "Vente" : "Achat"}</td>}
            {searchElt("created_at") && <td>{date.toLocaleDateString("fr-FR")}</td>}
            {searchElt("product_id") && <td>{order.product_id}</td>}
            {searchElt("size") && <td>{displayRoundWith10ToMinus(parseFloat(order.size),3)}{getSymbol(curr2)}</td>}
            {searchElt("price") && <td>{Math.round(parseFloat(order.price)*rounded)/rounded}{getSymbol(curr1)}</td>}

            {searchElt("globalValue") && <td>{ order.side === "sell" && order.globalValue  ? `${Math.round(parseFloat(order.globalValue)*100)/100}${getSymbol(curr1)}` : "--"}</td>}
            {searchElt("sellPrice") && <td>{ order.side === "sell" && order.sellPrice ?  `${Math.round(parseFloat(order.sellPrice)*100)/100}${getSymbol(curr1)}` : "--"}</td>}
            {searchElt("fee") && <td>{Math.round(parseFloat(order.fee)*rounded)/rounded}{getSymbol(curr1)}</td>}
            {searchElt("netSellPrice") && <td>{order.side === "sell" && order.netSellPrice ? `${Math.round(parseFloat(order.netSellPrice)*100)/100}${getSymbol(curr1)}` : "--"}</td>}
            {searchElt("buyCost") && <td>{ order.side === "sell" && order.buyCost ? `${Math.round(parseFloat(order.buyCost)*100)/100}${getSymbol(curr1)}` : "--"}</td>}
            {searchElt("quotaInit") && <td>{ order.side === "sell" && order.quotaInit ? `${Math.round(parseFloat(order.quotaInit)*100)/100}${getSymbol(curr1)}` : "--"}</td>}
            {searchElt("netBuyCost") && <td>{ order.side === "sell" && order.netBuyCost ? `${Math.round(parseFloat(order.netBuyCost)*100)/100}${getSymbol(curr1)}` : "--"}</td>}
            {searchElt("gain") && <td>{order.side === "sell" && order.gain ? `${Math.round(order.gain *100)/100}${getSymbol(curr1)}` : "--"}</td>}
            {actions && <td className={"action-cell"}>{order.user_id === undefined ? <HistoryAction onDelete={() => actions.delete(order.trade_id, order.product_id)} onUpdate={() => actions.update(order, 'update')} dataKey={order.trade_id} /> : "--"}</td>}
        </tr> )
    });

    return (
        <div className={`history-table ${responsive ? "table-responsive-perso" : ""} ${className ? className : ""}`}>
            <div className={"history-action"}>

                {printable &&
                <ReactToPrint
                    trigger={() => <button title={"Imprimer"} className={"action-btn btn btn-primary"} style={{float: "right"}}>
                        <PrintIcon/>
                    </button>}
                    content={() => printable.current}
                    copyStyles={true}
                    onBeforePrint={() => document.title = `Historique - - ${printTitle}`}
                    onAfterPrint={() => document.title = originalTitle}
                />}
                {children}
            </div>
            <div>
                {totalSection}
            </div>
            <table>
                <thead>
                <tr>
                    {theads}
                    {actions && <th className={"action-col"}>Actions</th>}
                </tr>
                </thead>
                <tbody>
                    {orderList}
                </tbody>
            </table>
        </div>
    )
};

export default OrderTable;