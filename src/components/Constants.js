
export const supportedPairs = [
    "ETH-EUR",
    "BTC-EUR",
    "XLM-EUR",
    "XTZ-BTC"
];

export const currencies = [
    {curr: "EUR", sym: "€", type: "FIAT"},
    {curr: "USD", sym: "$", type: "FIAT"},
    {curr: "USDT", sym: "\u20AE", type: "CRYP"},
    {curr: "ETH", sym: '\u039E', type: "CRYP"},
    {curr: "BTC", sym: '\u0243', type: "CRYP"},
    {curr: "XLM", sym: '\u00F8', type: "CRYP"},
    {curr: "XTZ", sym: '\uA729', type: "CRYP"},
    {curr: "BITG", sym: 'BG', type: "CRYP"},
];