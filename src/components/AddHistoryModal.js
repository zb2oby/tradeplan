import React, {useState, useEffect} from 'react';
import {currencies, supportedPairs} from "./Constants";
import {hideModal} from "../utils";
import {Form, FormItem, Select} from 'react-form-lib';
import {historyFormRules} from "../constants/validationRules";

const AddHistoryModal = ({toUpdate, onSave, type}) => {

    const [side, setSide] = useState('');
    const [size, setSize] = useState('');
    const [price, setPrice] = useState('');
    const [fee, setFee] = useState('');
    const [created, setCreated] = useState('');
    const [productId, setProductId] = useState('');
    const [currency, setCurrency] = useState('');
    const [thing, setThing] = useState('');
    const [description, setDescription] = useState('');

    useEffect(() => {

        if (type === 'update') {
            if (toUpdate.side === "sell" && toUpdate.sellType === "external") {
                setSide("external");
                setCurrency(toUpdate.currency);
                setThing(toUpdate.thing);
                if (toUpdate.description) {
                    setDescription(toUpdate.description);
                }
                let p = parseFloat(toUpdate.price) * parseFloat(toUpdate.size);
                setPrice(p.toString());
            } else {
                setSide(toUpdate.side);
                setPrice(toUpdate.price);
            }
            setSize(toUpdate.size);
            setFee(toUpdate.fee);
            setProductId(toUpdate.product_id);

            let dateSplitted = toUpdate.created_at.split("T");
            let date = dateSplitted[0];
            let hourSplitted = dateSplitted[1].split(":");
            let hour = hourSplitted[0] + ":" + hourSplitted[1];
            let finalField = date + "T" + hour;

            setCreated(finalField);
        } else {
            resetLocalData();
        }


    }, [toUpdate, type]);


    const resetLocalData = () => {
        setSide('');
        setSize('');
        setPrice('');
        setFee('');
        setCreated('');
        setProductId('');
        setThing('');
        setCurrency('');
        setDescription('');

    };


    const submit = (dat) => {

        if (dat.side === "external") {
            dat.side = "sell";
            dat.sellType = "external";
            dat.price = dat.price / dat.size;
        }

        return {...dat, trade_id: 0, created_at: dat.created_at + ":00.000Z"};
    };

    const handleSubmit = (data) => {
        onSave(submit(data));
        hideModal("insert-history");
    };

    const optionsPair = supportedPairs.map((p,i) => ({label: p, value: p}));
    let optionsCurrency = [{label: "Autre", value: "autre"}];
    currencies.forEach((c,i) => (optionsCurrency.push({label: c.curr, value: c.curr})));
    let optionsThing = [{label: "Autre (bien, service...)", value: "other"}, {label: "Autre crypto", value: "otherCurr"}];
    currencies.forEach((c,i) => (optionsThing.push({label: c.curr, value: c.curr})));
    const optionsSide = [{label: "Achat", value: "buy"}, {label:"Vente", value:"sell"}, {label: "Cession externe", value: "external"}];


    return (
        <div className="modal fade" id={`insert-history`} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-header">
                        <h6 className="modal-title" id="exampleModalLongTitle">{toUpdate !== '' ? "Modifier un ordre" : "Ajouter un ordre"}</h6>
                    </div>
                    <div className="modal-body">

                        <Form validationRules={historyFormRules} >

                            <FormItem
                                name={"created_at"}
                                label={"Date"}
                                required={true}
                                initialValue={created}
                                show={() => true}
                                type={"text"}
                                className={"form-group"}
                            >
                                <input type="datetime-local" className={"form-control"}/>
                            </FormItem>

                            <FormItem
                                name={"side"}
                                label={"Type"}
                                show={() => true}
                                initialValue={side}
                                callBackAction={(v) => setSide(v.side)}
                                required={true}
                                type={"select"}
                                className={"form-group"}
                            >
                                <Select placeHolder={"Sélectionnez un type"} dataSet={optionsSide} className={"form-control"} />
                            </FormItem>

                            <FormItem
                                name={"product_id"}
                                label={`${side === "external" ? "Paire impactée" : "Paire"} `}
                                sublabel={`${side === "external" ? "Définissez la paire d'echange que vous souhaitez impacter par cet enregistrement" : ""} `}
                                show={() => true}
                                initialValue={productId}
                                required={true}
                                type={"select"}
                                className={"form-group"}
                            >
                                <Select placeHolder={"Sélectionnez une paire"} dataSet={optionsPair} className={"form-control"} />
                            </FormItem>

                            <FormItem
                                name={"currency"}
                                label={"Actif"}
                                show={(v) => side === "external"}
                                initialValue={currency}
                                required={true}
                                type={"select"}
                                className={"form-group"}
                            >
                                <Select placeHolder={"Sélectionnez l'actif cédé"} dataSet={optionsCurrency} className={"form-control"} />
                            </FormItem>
                            <FormItem
                                name={"thing"}
                                label={"Objet de la cession"}
                                show={(v) => side === "external"}
                                initialValue={thing}
                                required={true}
                                type={"select"}
                                className={"form-group"}
                            >
                                <Select placeHolder={"Sélectionnez l'objet de la cession"} dataSet={optionsThing} className={"form-control"} />
                            </FormItem>

                            <FormItem
                                name={"description"}
                                label={"Description - Commentaire"}
                                show={(v) => side === "external"}
                                initialValue={description}
                                type={"text"}
                                className={"form-group"}
                            >
                                <textarea cols={10} rows={2} className={"form-control"}/>
                            </FormItem>

                            <FormItem
                                name={"size"}
                                label={`${side === "external" ? "Taille cédée" : "Taille"} `}
                                required={true}
                                initialValue={size}
                                show={() => true}
                                type={"text"}
                                className={"form-group"}
                            >
                                <input type="number" className={"form-control"}/>
                            </FormItem>
                            <FormItem
                                name={"price"}
                                label={`${side === "external" ? "Valeur de l'objet" : "Prix"} `}
                                required={true}
                                initialValue={price}
                                show={() => true}
                                type={"text"}
                                className={"form-group"}
                            >
                                <input type="number" className={"form-control"}/>
                            </FormItem>
                            <FormItem
                                name={"fee"}
                                label={"Frais"}
                                required={true}
                                initialValue={fee}
                                show={() => true}
                                type={"text"}
                                className={"form-group"}
                            >
                                <input type="number" className={"form-control"}/>
                            </FormItem>
                            <div className={"btn-toolbar modal-footer"} role={"toolbar"}>
                                <FormItem
                                    show={() => true}
                                    callBackAction={handleSubmit}
                                    type={"submit"}
                                >
                                    <button type="button"  className="btn btn-primary">{toUpdate !== ''  ? "Modifier" : "Ajouter"}</button>
                                </FormItem>
                                <FormItem
                                    show={() => true}
                                    type={"reset"}
                                >
                                    <button id="dismiss-insert-history" type="button"  className="btn btn-primary" data-dismiss="modal">Annuler</button>
                                </FormItem>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default AddHistoryModal;