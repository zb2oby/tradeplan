import React, {useEffect, useState} from 'react';
import {hideModal} from "../utils";
import {Form, FormItem, Select} from 'react-form-lib';
import {tradePlanFormRules} from "../constants/validationRules";


const TradePlanModal = ({tpType, pair, portfolio, onSubmit, dataKey, toUpdate}) => {

    const [type, setType] = useState('');
    const [quantite, setQuantite] = useState('');
    const [prix, setPrix] = useState('');
    const [inputInfo, setInputInfo] = useState(null)

    useEffect(() => {

        if (tpType === "update" && toUpdate !== '') {
            setType(toUpdate.type);
            setQuantite(toUpdate.quantite);
            setPrix(toUpdate.prix);
            calcInputInfo(toUpdate.quantite, toUpdate.type)
        } else {
            setPrix('');
            setQuantite('');
            setType('');
            setInputInfo(null);
        }
    }, [toUpdate, tpType]);

    const submit = (dat) => {

        let data = {...dat, dataKey: dataKey};
        setPrix('');
        setQuantite('');
        setType('');
        setInputInfo(null);
        return data;

    };


    const handleSubmit = (data) => {
        onSubmit(tpType, submit(data))
        hideModal("addTradePlan");
    };


    const calcInputInfo = (quantite, type) => {
        const inputInfo = (type !== '' &&  ((type === "sell" && portfolio.curr2 === 0) || (type === "buy" && portfolio.curr1 === 0)) ) ?
            <label className={"text-danger small"}>Plus de disponibilité en {type === "sell" ? pair.curr2 : pair.curr1}</label>
            :
            <label className={"small"}>{`${(Math.round(quantite * (type === "sell" ? portfolio.curr2 : portfolio.curr1) * 10)/ 1000)} ${type === "sell" ? pair.curr2 : pair.curr1}`}</label>

        setInputInfo(inputInfo)
    };


    let modalTitle = '';
    if (tpType === "update") {
        modalTitle = "Modification d'un prévision";
    } else if (tpType === "above") {
        modalTitle = "Insertion d'une prévision au dessus";
    } else {
        modalTitle = "Ajout d'une prévision";
    }

    const optionsType = [{label: "Achat", value: "buy"}, {label: "Vente", value: "sell"}];

    return (
        <div className="modal fade" id="addTradePlan" tabIndex="-1" role="dialog">
            <div className="modal-dialog modal-dialog-centered" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h6 className="modal-title" id="exampleModalLongTitle">{modalTitle}</h6>
                        Disponibilités : {Math.round(portfolio.curr1*100)/100}{pair.curr1} -- {Math.round(portfolio.curr2*100)/100}{pair.curr2}
                    </div>
                    <div className="modal-body">
                        {tpType === "above" && <p className={"text-warning"}>Vous allez ajouter une prévision entre deux existantes. Veuillez noter que l'ajout d'une prévision à cet endroit va entrainer un recalcul des prévisions plus récentes. Les disponibilités affichées ici correspondent donc aux disponibilités précédentes.</p>}
                        {tpType === "update" && <p className={"text-warning"}>Vous allez modifier une prévision existante. Veuillez noter que cette modification va entrainer un recalcul des prévisions plus récentes. Les disponibilités affichées ici correspondent donc aux disponibilités précédentes.</p>}

                        <Form validationRules={tradePlanFormRules} >
                            <FormItem
                                name={"type"}
                                label={"Type"}
                                required={true}
                                initialValue={type}
                                type={"select"}
                                callBackAction={(values) => calcInputInfo(values.quantite, values.type)}
                                show={() => (true)}
                                className={"form-group"}
                            >
                                <Select placeHolder={"Sélectionnez un type"} dataSet={optionsType} className={"form-control"} />
                            </FormItem>

                            <FormItem
                                name={"quantite"}
                                label={"Quantité (% des disponibilités)"}
                                inputInfo={inputInfo}
                                required={true}
                                initialValue={quantite}
                                callBackAction={(values) => calcInputInfo(values.quantite, values.type)}
                                show={(values) => values.type !== ""}
                                type={"text"}
                                className={"form-group"}
                            >
                                <input type="number" min={0} max={100} disabled={(type === "sell" && portfolio.curr2 === 0) || (type === "buy" && portfolio.curr1 === 0) } className={"form-control"}/>
                            </FormItem>

                            <FormItem
                                name={"prix"}
                                label={"Prix"}
                                required={true}
                                initialValue={prix}
                                type={"text"}
                                className={"form-group"}
                                show={(values) => (values.type !== "" && (values.type === "sell" && portfolio.curr2 !== 0) || (values.type === "buy" && portfolio.curr1 !== 0))}
                            >
                                <input type="number" className={"form-control"}/>
                            </FormItem>

                            <div className={"btn-toolbar modal-footer"} role={"toolbar"}>
                                <FormItem
                                    show={() => (true)}
                                    callBackAction={handleSubmit}
                                    type={"submit"}
                                >
                                    <button type="button"  className="btn btn-primary">{tpType === "update" ? "Modifier" : "Ajouter"}</button>
                                </FormItem>
                                <FormItem
                                    show={() => true}
                                    type={"reset"}
                                >
                                    <button id="dismiss-addTradePlan" type="button"  className="btn btn-primary" data-dismiss="modal">Annuler</button>
                                </FormItem>
                            </div>
                        </Form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default TradePlanModal;