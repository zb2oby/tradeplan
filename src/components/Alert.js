import React from 'react';

const Alert = ({reason, message, onDismiss, type, className}) => {

    return (
        <div className={`alert alert-${type} alert-dismissible fade show ${className}`} role="alert">
            <strong>{reason}</strong> {message && message}
            {onDismiss &&
            <button type="button" className="close" data-dismiss="alert" onClick={onDismiss ? () => onDismiss(null): null} aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>}
        </div>
    )
}

export default Alert;