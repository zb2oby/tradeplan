import React from 'react';

const ConfirmDeleteModal = ({onDelete, dataKey, lastOrder, deleteButtonTxt, cancelButtonTxt, children}) => {

    return (
        <div className="modal fade" id={`confirm-delete-${dataKey}`} tabIndex="-1" role="dialog" aria-labelledby="myModalLabel"
             aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered">
                <div className="modal-content">
                    <div className="modal-body">
                        <p>
                            {children ? children : "Souhaitez-vous vraiment supprimer cet ordre ?"}
                        </p>
                        <p className={"text-warning"}>
                            {lastOrder !== undefined && !lastOrder && "Veuillez noter que la suppression de cette prévision va entrainer un recalcul des prévisions plus récentes."}
                        </p>
                    </div>
                    <div className="modal-footer">
                        <button type="button"  className="btn btn-primary" data-dismiss="modal">{cancelButtonTxt}</button>
                        <button type="button"  className="btn btn-danger" data-dismiss="modal" onClick={onDelete}>{deleteButtonTxt}</button>
                    </div>
                </div>
            </div>
        </div>
    )
};

export default ConfirmDeleteModal;